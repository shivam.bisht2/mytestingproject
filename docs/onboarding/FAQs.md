![](img/faq.gif)

## Software Catalog

**I am unable to view my Catalog Entity in Backstage after registering my `catalog-info.yaml` and get a `Conflict Error` with `Status Code 409` on re-registering the component. What am I doing wrong?**

The most common reason for getting this issue is that you are registering a catalog from a branch with `/` in its name.

Backstage's URL processor is unable to handle such branch names. Read [Importing a Software Catalog](10-import-catalog.md) documentation for more information.

**How is Ownership and Identity Resolution managed with Backstage?**

Ownership and Identity Resolution of a Backstage catalog item with a user is with the prefix of `William Hill` email associated with the GitLab account.

For example, if your mail is `john.doe@williamhill.co.uk` - Backstage renders **Owned** items which are either owned by:
- owned by the user `john.doe` or 
- the group/department in which the user is a member

**No items are shown under the "Owned" List. How do I fix this?**

If you are getting no owned items on Backstage, either of the following cases might be true:

* Your user/team is not onboarded on Backstage
    * Onboard Users/Team by defining them as Backstage Users and Groups from [here](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/catalog/william-hill/org.yaml)
    * Request onboarding users/team by raising an [issue](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/issues) 
* Your user is onboarded incorrectly - Make sure that your onboarded user definition has `metadata.name` that matches the prefix of your email address.

## Software Entities (kind: Component)

**What are the suggested Types of Components that I can onboard onto Backstage?**

While defining a Software Entity of `kind: Component`, we specify their type in the `spec.type` field. We recommend the following three values:

- [x] service
- [x] website
- [x] library

Read the [Software Component](4-Software-Catalog.md) documentation for more information.

## TechDocs

**Are any extensions and plugins supported in TechDocs?**

Currently, we only support the following plugins,

- [x] PlantUML 

We plan to support the following plugins/extensions in the future.

- [x] Kroki
- [x] Draw.io
- [x] Mermaid

To keep a track of the progress, you can follow this [JIRA Ticket](https://jira.willhillatlas.com/browse/PEM-851).

## API (kind: API)

**Can I onboard an OpenAPI Definition distributed into multiple files?**

No, we only support OpenAPI definitions in a standalone specification file.

The support for distributed API Definition is currently in the [works](https://github.com/backstage/backstage/issues/6832).

## Templates (kind: Template)

**What steps should I follow in order to unregister my Software Template from Backstage?**

There is no direct `Unregister-Template` options available currently.

Please follow the steps mentioned in this [document](unregister-template.md) to unregister your templates.



