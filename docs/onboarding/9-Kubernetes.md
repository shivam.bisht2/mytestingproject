![](img/kuberentes-catalog.gif)

Backstage offerings for Kubernetes have enhanced support for developers where they can attach Kubernetes resources like Pods, Services, etc., and view them at Backstage.

A Software Entity of `kind: Component` registered with `spec.type: service` can be associated with Kubernetes resources. 

![](img/k8s-1.png)

To enable this feature, let's understand this plugin in-depth.

## Associating Kubernetes Resources with Software Component

Backstage supports linking specific Kubernetes resources to the Software Catalogs rather than viewing the entire Cluster details in a single location ( We already have Kubernetes dashboard for that!! )

For this, we have to onboard the Kubernetes cluster in Backstage.

## Onboarding Kubernetes cluster in Backstage

Unfortunately, Backstage currently has no support for onboarding clusters via UI.

We can only onboard Kubernetes clusters via Backstage Configuration's file `app-config.yaml`

For this, you would have to share the cluster details with the maintainers of WH Backstage or submit a Merge Request to this [file](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/portal-app/app-config.yaml).

To onboard a cluster on Backstage, add the following descriptor in `kubernetes.clusterLocatorMethods` in `app-config.yaml`

```yml
kubernetes:
  objectTypes: ["configmaps","deployments","horizontalpodautoscalers","ingresses","pods","replicasets","services"]
  serviceLocatorMethod:
    type: 'multiTenant'
  clusterLocatorMethods:
    - type: 'config'
      clusters:
        - url: [url of the hosted kubeconfig]
          name: [cluster-name]
          authProvider: ['aws','serviceAccount']
          skipTLSVerify: [true/false]
          serviceAccountToken: []
        - ...
```

To make these changes in the `app-config.yaml` we require the following:

- [x] Share the API Server Endpoint of the cluster: `$ kubectl cluster-info`
- [x] A Service Account for Authentication - for more information, ping us at [#platform-engineering-portal-community](https://williamhill.slack.com/messages/platform-engineering-portal-community/)
- [x] Add Security Group Inbound Rules to allow Backstage to interact with your clusters via Istio

### Creating a Service Account for Authentication

```yml
kind: ServiceAccount
apiVersion: v1
metadata:
  name: backstage
  namespace: sit
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: backstage
rules:
  - apiGroups: ["", "apps", "autoscaling", "networking.k8s.io"]
    resources:
      [
        "pods",
        "namespaces",
        "services",
        "configmaps",
        "deployments",
        "replicasets",
        "horizontalpodautoscalers",
        "ingresses",
      ]
    verbs: ["get", "watch", "list"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: backstage
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: backstage
subjects:
- kind: ServiceAccount
  name: backstage
  namespace: sit
```

### Fetching the Service Account Token

```
kubectl get secret $(kubectl get sa <SERVICE_ACCOUNT_NAME> -o=json \
| jq -r '.secrets[0].name') -o=json \
| jq -r '.data["token"]' \
| base64 --decode \
| pbcopy
```

Please share this token with the William Hill Engineering Portal Maintainers for encryption and storage of the same in the Backstage configuration.

## Associating Kubernetes Resources with Backstage Components

There are two ways to associate Kubernetes resources with Backstage Components:

- [x] Matching Kubernetes Resource's `metadat.labels` with Backstage Component's `metadata.annotations`
- [x] Label Selector Query

### Linking Kubernetes resources with the Service Catalog Components

* Catalog Descriptor file for Backstage Entity:

```yml
metadata:
  annotations:
    'backstage.io/kubernetes-id': [value]
    ...
```

This should match the `metadata.labels` in the definition of Kubernetes resource:

```yml
metadata:
  labels:
    'backstage.io/kubernetes-id': [value]
```

### Fetching Kubernetes Resources with Label Selector Query

Backstage comes with a [label selector query](https://backstage.io/docs/features/kubernetes/configuration#label-selector-query-annotation) that helps us link Kubernetes resources with the Software Entity without matching Kubernetes resource's `metadata.labels` with catalog's `metadata.annotations`

## Viewing the Kubernetes Plugin in Action

Once you have successfully onboarded the Kubernetes cluster and attached resources with the Software Component, you can view the plugin in action on the `Kubernetes` Tab.

![](img/kube.png)