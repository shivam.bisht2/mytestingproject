![](img/ownership.gif)

Backstage captures the Identity of Groups and Users (members of groups) and assigns Ownership of onboarded catalog items with them.

This eco-system of groups and users can be viewed within [Backstage](https://backstage.uat.platform-eng.aws-ew1.dv.williamhill.plc/explore/groups).

## Registering your User / Group in Backstage

While you are free to create catalog descriptor files that define your Groups and Users and store them in your own GitLab repositories and onboard them yourselves, for the sake of centralization, we recommend you to create your groups and users in the [Backstage's maintainer repository](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/tree/main/catalog/william-hill).

## Groups In Backstage

Describes the following Entity Type:

| **Field** | **Value** |
|---|---|
| apiVersion | backstage.io/v1alpha1 |
| kind | Group |

A group defines a logical collection of Users/Engineers onboarded onto Backstage. It can be an organization, a department, a team, etc.

Backstage creates a tree structure of the Groups with `parent` and `child` relationships.

### Defining Groups with Catalog Descriptor

We define a Group in Backstage with the following YAML descriptor file:

```yml
apiVersion: backstage.io/v1alpha1
kind: Group
metadata:
  name: [group name]
  description: [description of the group]
spec:
  type: [value] # Required Field
  profile: # Optional Field
    displayName: [value]
    email: [group email]
    picture: [URL to image]
  parent: [array of parent groups] # Optional
  children: [array of child groups] # Required, [] if empty
  members: [array of users onboarded in backstage] # Optional Field
```

**Note:** `spec.parent` , `spec.children` and `spec.members` should only include values that match `metadata.name` of groups and users defined in Backstage.

### `spec.type` 

There is no enforcing of values for this but we recommend using the following commonly used values

- [x] team
- [x] department
- [x] sub-department
- [x] channel

### Sample Group Onboarded in Backstage

**Example Group Onboarded on Backstage:** [CPE Team](https://backstage.uat.platform-eng.aws-ew1.dv.williamhill.plc/catalog/default/group/cpe)

![](img/group.png)

## Users in Backstage

Describes the following Entity Type:

| **Field** | **Value** |
|---|---|
| apiVersion | backstage.io/v1alpha1 |
| kind | User |

A user describes a person, such as an employee, a contractor, or similar. 

Users belong to Group entities in the catalog.

### Defining Users with Catalog Descriptor

```yml
apiVersion: backstage.io/v1alpha1
kind: User
metadata:
  name: [name]
spec:
  profile: # Optional
    displayName: [name]
    email: [email]
    picture: [url to image]
  memberOf: [array of groups a user belongs in] # Required Field, [] if empty
```

**Note:** Values in `spec.memberOf` must match Groups `metadata.name` that exists in Backstage.

### Sample User Onboarded in Backstage

**Example User Onboarded on Backstage:** [Nathan Flynn](https://backstage.uat.platform-eng.aws-ew1.dv.williamhill.plc/catalog/default/user/nflynn) 

![img](img/user.png)

