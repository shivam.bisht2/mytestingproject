![](img/api.gif)

| **Field** | **Value** |
|---|---|
| apiVersion | backstage.io/v1alpha1 |
| kind | API |

* To onboard an API in Backstage, we have to define it in a catalog descriptor file written in YAML with `kind: API`.
This definition must be stored on source control (GitLab for William Hill) and then registered in Backstage.

* Backstage has support for the following API formats:

- [x] OpenAPI
- [x] AsyncAPI
- [x] gRPC (Google Remote Procedure Calls)
- [x] GraphQL

## Standard Software Catalog for `kind: API`

```yml
apiVersion: backstage.io/v1alpha1
kind: API
metadata:
  name: [value]
  namespace: [value]
  description: [value]
  labels: 
    [key]: [value]
  tags: 
    - [value]
spec:
  type: [value]
  lifecycle: [value]
  owner: [value]
  system: [value]
  definition:
    [API Specification]
```

## Sample API Catalog descriptor onboard an OpenAPI Specification in Backstage

```yml
apiVersion: backstage.io/v1alpha1
kind: API
metadata:
  name: petstore
  description: Swagger Example PetStore API
spec:
  type: openapi
  lifecycle: production
  owner: nish
  definition:
    "$text" : "https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/api/petstore/petstore.json" 
```

To import an API in Backstage, follow this [document](10-import-catalog.md).

Once registered, we can view the list of APIs from the `API` sidebar.

![](img/petstore.png)

## Understanding the Schema and Semantics of Standard YAML Descriptor to onboard an API

### metadata.name

* Required field
* Should be unique for `API` kind throughout backstage if namespace is not defined, else must be unique within the namespace.
* Number of characters: `[1,63]`
* Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

### metadata.namespace

* Optional field
* Preferred if group name of WH Team
* Number of characters: `[1,63]`
* Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

### metadata.labels

* Optional field
* Key-value pairs
* Classify the component
* Reference component to other components
* Used to filter search/queries
* Labels are of format `key: value` where both keys and values are strings.
* Key is a combination of prefix and name of the key separated by `/`

* `Prefix` Rules:
- [x] lowercase characters only
- [x] Number of characters: `[1,253]`
- [x] Backstage keyword prefix `backstage.io/`

* `Name` and `Value` rules:

- [x] Number of characters: `[1,63]`
- [x] Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

### metadata.tags

* Optional field
* Classify components
* In this context, tags to define the repository
* Single valued strings

### spec.type

* required field - Name of the API format
* Supported values
  
- [x] openapi
- [x] grpc
- [x] asyncapi
- [x] graphql

### spec.owner

* required field
* Can be any string but should be standardized
* Commonly used values

- [x] Onboarded Group `group name`
- [x] Onboarded Team `team name` 
- [x] Onboarded User `user:username`
- [x] Onboarded Email: `fname.lname@williamhill.co.uk`

### spec.lifecycle

* required field
* Can be any string but should be standardized
* Commonly used values

- [x] `experimental`
- [x] `production`
- [x] `deprecated`

### spec.definition

* required field - the definition of the API
* Can take following formats:

- [x] Inline definition

```yml
definition: |
  ...
```

- [x] `$text` - rended API definition from generic file

```yml
definition: 
  $text: [relative path to file or URL]
```

- [x] `$json` - parse and validate definition written in json file

```yml
definition: 
  $json: [relative path to file or URL]
```

- [x] `$yaml` - parse and validate definition written in yaml file

```yml
definition: 
  $yaml: [relative path to file or URL]
```
