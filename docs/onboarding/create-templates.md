![](img/create-template.gif)

Templates are stored in the Software Catalog under a kind Template. You can create your templates with a small Yaml definition that describes the template and its metadata, along with some input variables that your template will need, and then a list of actions which are then executed by the scaffolding service

It would be good to also have some files in there that can be templated in. These files are stored inside /templates folder. Paste your `template.yaml` file inside the folder of your template(eg:react-ssr-app in the image below) along with the skeleton folder, skeleton folder contains files that can be templated in.

![](../img/vscode-explorer-ss.png)

A simple `template.yaml` definition might look something like this:(just a sample, please don't copy it ✌🏼):

```yml
apiVersion: backstage.io/v1beta2
kind: Template
metadata:
  name: react-ssr-template
  title: React SSR Template
  description: Create a website powered with Next.js(SSR)
  tags:
    - recommended
    - react
spec:
  owner: shivam.bisht@williamhill.co.uk
  type: website
  parameters:
    - title: Fill in template parameters
      required:
        - component_id
        - owner
      properties:
        component_id:
          title: Name
          type: string
          description: Unique name of the component
        description:
          title: Description
          type: string
          description: Help others understand what this website is for.
        owner:
          title: Owner
          type: string
          description: Owner of the component
          ui:field: OwnerPicker
          ui:options:
            allowedKinds:
              - Group
    - title: Choose a location
      required:
        - repoUrl
      properties:
        repoUrl:
          title: Repository Location
          type: string
          ui:field: RepoUrlPicker
          ui:options:
            allowedHosts:
              - gitlab.com
  steps:
    - id: template
      name: Fetch Skeleton + Template
      action: fetch:template
      input:
        url: ./skeleton
        values:
          component_id: '{{ parameters.component_id }}'
          description: '{{ parameters.description }}'
          destination: '{{ parseRepoUrl parameters.repoUrl }}'
          owner: '{{ parameters.owner }}'
          http_port: 8080

    - id: publish
      name: Publish
      action: publish:gitlab
      input:
        allowedHosts: ['gitlab.com']
        description: 'This is {{ parameters.component_id }}'
        repoUrl: '{{ parameters.repoUrl }}'

    - id: register
      name: Register
      action: catalog:register
      input:
        repoContentsUrl: '{{ steps.publish.output.repoContentsUrl }}'
        catalogInfoPath: '/catalog-info.yaml'

  output:
    remoteUrl: '{{ steps.publish.output.remoteUrl }}'
    entityRef: '{{ steps.register.output.entityRef }}'
```

`NOTE:` After you have created your template(react-ssr-template) inside the templates folder, you need to register it by navigating to `/catalog-import` or pressing ` REGISTER EXISTING COMPONENT` in Create a Component page on route `/create`

You can find your template on `Available Templates` page once you have registered it as shown in the image below.
![](../img/template-list.png)

### Description of the above sample Yaml

- metadata: Provides metadata about the template itself<br />
  metadata.name: Name of the component<br />
  metadata.title: Title that will be displayed on the template UI in backstage<br />
  metadata.description: Description that will be displayed on the template UI in backstage

- spec: <br />
  spec.owner: Owner information displayed in template UI<br />
  spec.type: Type of the template eg: library, service, website etc

- parameters: These are the steps that are rendered in the frontend with the form input and collect information about the component you want to create using template<br />
  parameters->title: It provides the form label to the UI which collects parameters/location information for the template at every step<br />
  parameters->required: It provides which of the fields are mandatory<br />
  parameters->properties: It provides the information about the type of data to be provided through input fields<br />

- spec.steps: The step is an array of the things that you want to happen part of this template. These follow the same standard format:

  ```yml
  - id: template
      name: Fetch Skeleton + Template
      action: fetch:template
      input:
        url: ./template
        values:
          component_id: '{{ parameters.component_id }}'
          description: '{{ parameters.description }}'
          destination: '{{ parseRepoUrl parameters.repoUrl }}'
          owner: '{{ parameters.owner }}'
          http_port: 8080
  ```

  NOTE: The steps are executed in series in the scaffolder backend

Sometimes, especially in custom fields, you collect some data on Create form that must not be shown to the user on the Review step. To hide or mask this data, you can use ui:widget: password or set some properties of ui:backstage:

```yml
- title: Hide or mask values
  properties:
    password:
      title: Password
      type: string
      ui:widget: password # will print '******' as value for property 'password' on Review Step
    masked:
      title: Masked
      type: string
      ui:backstage:
        review:
          mask: '<some-value-to-show>' # will print '<some-value-to-show>' as value for property 'Masked' on Review Step
    hidden:
      title: Hidden
      type: string
      ui:backstage:
        review:
          show: false # wont print any info about 'hidden' property on Review Step
```

## The Repository Picker

For ease of handling the repository providers easier, we've built a custom picker that can be used by overriding the ui:field option in the uiSchema for a string field. Instead of displaying a text input block, it will render the custom component that we've built which makes it easy to select a repository provider and insert a project or owner, and repository name.

You can see it in the above full example which is a separate step and it looks a little like this:

```yml
- title: Choose a location
  required:
    - repoUrl
  properties:
    repoUrl:
      title: Repository Location
      type: string
      ui:field: RepoUrlPicker
      ui:options:
        allowedHosts:
          - gitlab.com
```

The allowedHosts part should be set to where you wish to enable this template to publish. And it can be any host that is listed in your integrations config in app-config.yaml.

The RepoUrlPicker is a custom field that we provide as part of the plugin scaffolder. It's currently not possible to create your fields yet.

## The Owner Picker

When the scaffolder needs to add new components to the catalog, it needs to have an owner for them. Ideally, users should be able to select an owner when they go through the scaffolder form from the users and groups already known to Backstage. The OwnerPicker is a custom field that generates a searchable list of groups and/or users already in the catalog to pick an owner from. You can specify which of the two kinds are listed in the allowedKinds option:

```yml
owner:
  title: Owner
  type: string
  description: Owner of the component
  ui:field: OwnerPicker
  ui:options:
  allowedKinds: - Group
```

#### Outputs

Each step can output some variables that can be used in the scaffolder frontend after the job is finished. This is useful for things like linking to the entity that has been created with the backend and also linking to the created repository.

The main two that are used are the following:

```yml
output:
  remoteUrl: '{{ steps.publish.output.remoteUrl }}' # link to the remote repository
  entityRef: '{{ steps.register.output.entityRef }}' # link to the entity that has been ingested to the catalog
```

## Additional Fields available:

These fields are available exclusively for our backstage portal

### **AWS Tag Picker - Defined by DAVE tagging**
```yml
dataClassification:
  title: Data Classification
	type: string
	description: AWS Tag - Defined by DAVE tagging
	ui:field: AWSTagPicker
```

### **Custom drop down field** 

We can create a drop down field with custom options 

```yml
customDropDown:
	title: Customized Drop Down 
	type: string
	description: This Drop Down can be used with custom options 
	ui:field: CustomEntityPicker
	ui:options:
		customOptions:
			- Option 1
			- Option 2
			- Option 3
			- Option 4
```

### References

https://backstage.io/docs/features/software-templates/software-templates-index
