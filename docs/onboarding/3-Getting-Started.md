![](img/getting-started.gif)

Now that you are familiar with Backstage, let's start onboarding software entities onto Backstage.

Backstage supports onboarding the following software entities:

- [x] Repository
- [x] Documentation around the software with TechDocs
- [x] GitLab Plugin
- [x] API Definition
- [x] Kubernetes Resources attached with the Software Component
- [x] Templates
- [x] Groups
- [x] Users
- [x] Domain
- [x] System
- [x] Location
- [x] Resource

The amazing feature in Backstage is that every software entity is defined in a `Software Catalog Entity Descriptor file` written in YAML. These files must be stored on your VCS platform (GitLab, for William Hill) which is integrated with Backstage.

A Catalog lifecycle in Backstage follows this journey:

![](img/updated-catalog.png)

1. User defines a software entity in a catalog descriptor file written in YAML
2. User stores the catalog file in GitLab (or any other supported VCS)
3. User copies the URL from GitLab
4. User registers the Software Catalog Component via the Backstage User Interface
5. Backstage stores a location reference of the URL of the catalog from GitLab in the DB (Postgres/SQlite)
6. Backstage checks for changes in catalog descriptor in GitLab periodically
7. If any changes are made in catalog descriptor, it is synchronized in Backstage.

To import a software catalog in Backstage, follow this [document](10-import-catalog.md).

## Different Shapes and Semantics of Software Catalog in Backstage

| **Software Entity**  | **Kind**  | **Description** | 
|---|---|---|
| Software Components | `Component` | Includes Repositories, TechDocs, GitLab Plugin with optional support for Kubernetes Plugin, API Docs, and dependencies |
| Template | `Template` | Create Repositories, Infrastructure, etc. by filling a form on Backstage |
| API | `API` | Onboard API definitions with supported formats: OpenAPI, gRPC, GraphQL and AsyncAPI |
| Group of Users/Engineers | `Group` | Used to create a grouping of Backstage Users - Team, department, organization etc. |
| Users | `User` | Defines a user, customer, engineer, contractor etc. |
| Resources* | `Resource` | Infrastructure resources like S3 buckets, CDNs, etc. |
| System* | `System` | Collection of `Component` and `Resources` |
| Domains* | `Domain` | Collection of `System` |
| Locations* | `Location` | A location is a marker that references other places to look for catalog data |

`*` These software catalogs have not been explored yet on William Hill Engineering Portal but you can create use them by following the official [Backstage documentation](https://backstage.io/docs/features/software-catalog/descriptor-format).

**Standard YAML Envelope for a Backstage Entity** 

* `apiVersion` and `kind`

`kind` is the high-level entity description of the entity

`apiVersion` is the version of specification format for a software component that the specification is made against.

* `metadata`

A structure that contains metadata about the entity.

* `spec`

The specification details that describe the software entity.

### High Level Modelling Architecture of Software Entities in Backstage

![](img/backstage-overview.png)

Image reference: https://roadie.io/blog/modeling-software-backstage/


