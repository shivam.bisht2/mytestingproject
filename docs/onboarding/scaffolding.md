![](img/scaffold.gif)

Let's create Software Entities with existing Templates on Backstage.

To list the available templates, go to the `Homepage` and click on the `Create Component` button on the top-right corner of the screen.

![](../img/available-templates.png)

## Choose a template

After you select a template you'll be taken to the template inputs page.
Each template asks for specific input variables, and they are then consumed by the templating engine to produce the final output.

![](./img/template-picked.png)

After filling in these variables, you'll get some more fields to fill out which are required for Backstage usage: the owner (which is a user in the backstage system), and the storePath which is a destination URL to create for the provider, for instance https://gitlab.com/myteam/myrepo

![](./img/template-picked-2.png)

After Providing information about your component, review the information provided, and then you can click `CREATE` button as shown in the image below.
![](./img/template-picked-review.png)

#### Run!

Once you've entered values and confirmed them, you will be redirected to the task activity page with live progress of what is currently happening with the creation of your template.

![](./img/fetch-template-step.png)
![](./img/publish-template-step.png)

It shouldn't take too long, and you'll have a success screen!

![](./img/register-template-step.png)

When it's been created, then you'll also be able to see it in the Catalog View table,click on the component name you just created and you will be able to go over your component's screen.

![](./img/created-component.png)

### References

https://backstage.io/docs/features/software-templates/software-templates-index
