![](img/import-catalog.gif)

To onboard any software entity on Backstage, we follow these four simple steps:

1. Define the Software Entity Catalog Descriptor in YAML format
2. Store the catalog file on GitLab 
3. Copy the URL to the catalog file on GitLab - `https://www.gitlab.com/[project-slug]/-/blob/[branch name]/[path to catalog file]`
4. Register the Software Entity with its descriptor file in Backstage 

**Note: Once a catalog is registered onto Backstage, it polls periodically (one minute, by default) to capture any changes to the catalog definition and synchronizes them onto Backstage automatically.**

Let's see this workflow in action where we demonstrate to you how have we onboarded our [repository](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal) on Backstage

## Step 1: Create the Software Catalog Entity Descriptor file

**Note:** While you are free to name your catalog file anything you want, we recommend you to use the file name `catalog-info.yaml`

```yml
apiVersion: backstage.io/v1alpha1
kind: Component
metadata:
  name: platform-engineering-portal
  description: |
    William Hill Engineering Portal
  tags:
    - backstage
    - portal
    - william-hill
  links:
    - title: Backstage Platform
      url: https://backstage.io/
      icon: http
  annotations:
    gitlab.com/project-slug: williamhillplc/platform-engineering/portal/platform-engineering-portal
    backstage.io/techdocs-ref: dir:.
    gitlab.com/project-id: '28282364'
    backstage.io/kubernetes-id: "eks01"
spec:
  type: service
  lifecycle: experimental
  owner: portal
```

## Step 2: Store your Catalog Descriptor file on Version Control (GitLab for William Hill)

## Step 3: Copy your URL to the Catalog file from GitLab

Catalog files stored in GitLab have the following URL pattern:

`https://www.gitlab.com/[project-slug]/-/blob/[branch name]/[path to catalog file]`

## Step 4: Register your Catalog Entity in Backstage via UI

* Open Backstage on the Browser. Click on the `Create` Button on the Sidebar.

![](img/register-1.png)

* Click on `Register Existing Component` on the top right.

![](img/register-2.png)

* A Registration page for the software catalog opens up.

![](img/register-3.png)

* Paste your URL and click on `Analyze`. Backstage validates the semantics and schema of the catalog descriptor file.

Post successful analysis, click on `Import` to register the catalog into Backstage.

![](img/register-4.png)

![](img/register-5.png)

Once registered, you can view your Entity at Backstage.

![](img/registered-1.png)

Click on the registered catalog to view all supported features in action.

![](img/registered-2.png)

### Bug: Cannot Import Catalog from branches with `/` in the name

**Note: Backstage's URL Processor cannot read from branches with `/` in their names. It's a known issue and we will share the news of its support when it is available.**

If you onboarding software entities from branches with `/`; Backstage doesn't explicitly give an error message on the `Analyze` page but the component in not registered and visible on Backsatge. If you try to re-register the catalog item, you will get the following error:

![](img/branch-with-slash.png)

