![](img/templates.gif)

The Software Templates part of Backstage is a tool that can help you create Components inside Backstage.

Our goal is to make developer's life easier and happier. For this, Backstage Software Templates can give us a hand. This tool can help us create Components inside Backstage that will load code skeletons, fill in some variables and publish that template to some locations like GitLab.

To get the list of the available templates, go to the `Home page and click on the `Create Component` button on the top-right corner of the screen.

![](../img/available-templates.png)

We can create components with `Templates` using the following methods:

- [x] Scaffolding entities with Available templates
- [x] Creating our custom templates and then creating a component from the custom template

You will discover more about templates in the upcoming documents.

A Template describes a skeleton for use with the Scaffolder.
A template definition describes both the parameters that are rendered in the frontend part of the scaffolding wizard, and the steps that are executed when scaffolding that component.

| **Field**  | **Value**            |
| ---------- | -------------------- |
| apiVersion | backstage.io/v1beta2 |
| kind       | Template             |

# Sample Template Descriptor file

```yml
apiVersion: backstage.io/v1beta2
kind: Template
metadata:
  name: react-ssr-template
  title: React SSR Template
  description: Create a website powered with Next.js(SSR)
  tags:
    - recommended
    - react
spec:
  owner: shivam.bisht@williamhill.co.uk
  type: website
  # these are the steps which are rendered in the frontend with the form input
  parameters:
    - title: Fill in template parameters
      required:
        - name
      properties:
        name:
          title: Name
          type: string
          description: Unique name of the component
          ui:autofocus: true
          ui:options:
            rows: 5
        owner:
          title: Owner
          type: string
          description: Owner of the component
          ui:field: OwnerPicker
          ui:options:
            allowedKinds:
              - Group
    - title: Choose a location
      required:
        - repoUrl
      properties:
        repoUrl:
          title: Repository Location
          type: string
          ui:field: RepoUrlPicker
          ui:options:
            allowedHosts:
              - gitlab.com
    # here's the steps that are executed in series in the scaffolder backend
  steps:
    - id: template
      name: Fetch Skeleton + Template
      action: fetch:template
      input:
        url: ./skeleton
        values:
          component_id: '{{ parameters.component_id }}'
          description: '{{ parameters.description }}'
          destination: '{{ parseRepoUrl parameters.repoUrl }}'
          owner: '{{ parameters.owner }}'
          http_port: 8080

    - id: publish
      name: Publish
      action: publish:gitlab
      input:
        allowedHosts: ['gitlab.com']
        description: 'This is {{ parameters.component_id }}'
        repoUrl: '{{ parameters.repoUrl }}'

    - id: register
      name: Register
      action: catalog:register
      input:
        repoContentsUrl: '{{ steps.publish.output.repoContentsUrl }}'
        catalogInfoPath: '/catalog-info.yaml'

  output:
    remoteUrl: '{{ steps.publish.output.remoteUrl }}'
    entityRef: '{{ steps.register.output.entityRef }}'
```

# Understanding the Standard YAML Structure

## apiVersion and kind [required]

Exactly equal to backstage.io/v1alpha1 and Template, respectively.

## metadata.title [required]

- The nice display name for the template as a string, e.g. React SSR Template. \* \* This field is required as is used to reference the template to the user instead of the metadata.name field.

## metadata.tags [optional]

A list of strings that can be associated with the template, e.g. ['recommended', 'react'].

This list will also be used in the frontend to display to the user so you can potentially search and group templates by these tags.

## spec.type [optional]

The type of component as a string, e.g. website. This field is optional but recommended.

The current set of well-known and common values for this field is:

service - a backend service, typically exposing an API
website - a website
library - a software library, such as an npm module or a Java library

### References

https://backstage.io/docs/features/software-catalog/descriptor-format#kind-template
