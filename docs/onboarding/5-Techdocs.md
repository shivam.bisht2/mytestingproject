![](img/techdocs.gif)

TechDocs is Backstages' homegrown docs-like-code solution. 

It allows engineers to write their documentation in Markdown files which live together with their code.
This documentation can be viewed at Backstage without moving to Source Control, and it provides a level of transparency across channels within the organization.

The best thing about TechDocs is that you don't have to publish changes to Backstage every time you change the documentation. Backstage keeps on polling the referenced catalog descriptor file periodically and synchronizes any change made to the VCS.

To use TechDocs, we have to define an annotation for it within our catalog descriptor file:

```yml
metadata:
  annotations:
    backstage.io/techdocs-ref: dir:.
```

where the `dir` key exposes the relative path to the Mkdocs definition file defined below:

TechDocs plugin is built on top of the [MkDocs](https://www.mkdocs.org/). To set up TechDocs, we have to define a `mkdocs.yml` file. This is the file that the annotation in the catalog descriptor file refers to.

**Note:** Backstage Catalog descriptor files are unique to the Backstage project and `mkdocs.yml` / `mkdocs.yaml` file are related to the MkDocs project. Don't confuse the standard YAML of Backstage with it.

TechDocs allows us to link different documents together and gives us a table of contents through which we can navigate. (***See the Example below***)

The documentation's navigation window must be defined in the `mkdocs.yml`.

## Standard MkDocs Descriptor Format

```yml
site_name: [value]
site_description: [value]

plugins:
  - techdocs-core
  - ...

nav:
  - Header:
    - Sub-heading: 'index.md'
  - Header:
    - Sub-heading: 'path to file'
    - Sub-heading: 'path to file'
    ...
  - Document: 'path to file'
  ...
```

**Note:** All the documents referenced within the MkDocs descriptor file take the base path `docs/` directory that exists in the same directory as the `mkdocs.yml` file. 

## Sample MkDocs Descriptor file

```yml
site_name: William Hill Engineering Portal
site_description: Documentation around the User and Developer Journey on the William Hill Engineering Portal.

plugins:
  - techdocs-core

nav:
  - Introduction:
    - Introduction To Backstage:  'index.md'
  - User Documentation:
    - Welcome to Backstage: 'onboarding/welcome-to-backstage.md'
    - Software Catalog:
      - Overview: 'onboarding/3-Getting-Started.md'
      - Identity and Ownership:
        - Register Users and Groups: 'onboarding/2-Register-Users-And-Groups.md'
      - API: 
        - Registering an API: 'onboarding/6-API-Onboarding.md'
      - Component :
        - Software Components: 'onboarding/4-Software-Catalog.md'
        - TechDocs: 'onboarding/5-Techdocs.md'
        - Software Component Dependencies: 'onboarding/managing-dependencies.md'
        - Associating API with Software Components: 'onboarding/7-Stitch-API-With-Catalog.md'
        - Associating Kubernetes Resources with Software Components: 'onboarding/9-Kubernetes.md'
      - Template:
        - Create Template Repositories and Services: 'onboarding/8-Template.md'
      - Importing a Software Catalog: 'onboarding/10-import-catalog.md'
      - Unregistering a Software Catalog: 'onboarding/11-unregister-catalog.md'
    - Frequently Asked Questions: 'onboarding/FAQs.md'
```

![](img/techdocs-example.png)

## Extensions to MkDocs

TechDocs support for the following plugins/extensions has been enabled:

- [x] PlantUML

### PlantUML

PlantUML is an open-source tool allowing users to create diagrams from plain text language. 

```
Bob -> Alice : hello
Alice -> Bob : hi
```

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```

Follow the official [PlantUML Reference](https://plantuml.com/) to understand its syntax in Markdown.