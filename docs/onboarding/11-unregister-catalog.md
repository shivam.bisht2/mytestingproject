![](img/unregister-catalog.gif)

To un-register a Software Entity from Backstage, we follow these steps:

1. Select the component you want to unregister from the list of onboarded items.

![](img/unregister-1.png)

2. Once you have selected the component you want to unregister, click on the vertical three-dot menu on the top-right of the component page and you will see a pop-up `Unregister Entity` 

![](img/unregister-2.png)

3. Click on `Unregister Entity` and a confirmation pop-up will appear on the screen, then click on the `UNREGISTER LOCATION` button in red color:

![](img/unregister-3.png)
