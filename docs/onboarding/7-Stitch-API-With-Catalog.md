![](img/associate-api.gif)

Software Entities onboarded with `spec.type: service` can be linked to APIs in Backstage that the service provides and consumes.

You might have seen the `View API` button in the `Overview` Section is disabled by default.

![](img/stitch-1.png)

It happens because we have not linked APIs (onboarded as `kind: API`) with the Service Catalog (onboarded as `kind: Component`)

Navigating to the `API` Section of the Catalog shows you that the software neither provides an API nor consumes one.

![](img/no-api.png)

## Associating APIs with the Software Component

There is no mandate that a software entity must be associated with an API onboarded onto Backstage.

All these scenarios can be true for a Software Entity:

1. Neither provides an API nor consumes one
2. Provides one or more API but does not consume one
3. Does not provide an API but consumes one or more of them
4. Provides and consumes one or more APIs 

To associating APIs with the Software Component, specify the `spec.providesApis` and `spec.consumesApis` in the catalog descriptor file.

```yml
spec:
  providesApis:
    - [value]
    - [value]
    - ...
  consumesApis:
    - [value]
    - [value]
    - ...
```

**APIs referenced in the `spec.providesApis` and `spec.consumesApis` must match an existing API onboarded on Backstage with the same `metadata.name`**

## Sample Catalog Descriptor with Linked APIs

```yml
apiVersion: backstage.io/v1alpha1
kind: Component
metadata:
  name: platform-engineering-portal
  description: |
    William Hill Engineering Portal
  tags:
    - backstage
    - portal
    - william-hill
  links:
    - title: Backstage Platform
      url: https://backstage.io/
      icon: http
  annotations:
    gitlab.com/project-slug: williamhillplc/platform-engineering/portal/platform-engineering-portal
    backstage.io/techdocs-ref: dir:.
    gitlab.com/project-id: '28282364'
spec:
  type: service
  lifecycle: experimental
  owner: portal
  providesApis:
    - sessions-api
    - map-api
  consumesApis:
    - audit-api
```

After making the changes, wait for few minutes until Backstage synchronizes with the catalog file on the source control. Alternatively, you can unregister the catalog and re-register for instant changes.

Once refreshed, we can see that the `View API` Button is enabled.

![](img/stitch-4.png)

On Click, The View API button navigates us to the `API` Page to view the different APIs the software catalog provides and consumes.

![](img/stitch-5.png)

On clicking an API, it would lead us to the onboarded API definition page.

![](img/stitch-6.png)
