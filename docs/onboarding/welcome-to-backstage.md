![](img/welcome.gif)

We are pleased to have you on board in the Early Access to Backstage - a Developer Portal we plan to adopt as the single pane of glass across all channels in William Hill.

:warning: **This UAT environment is not intended to be the Production environment.**

**We are still investigating the environment strategy and promotion of products/services between different Backstage instances.**

**All Software catalog entities you onboard will not be automatically promoted once we open the production environment to all the users.**

## Features Available in Current Offerings

- [x] Software Catalog

* View Source
* Attach TechDocs
* GitLab Plugin for viewing Pull Requests, Pipelines, Contributors, and more 

- [x] API Documentation

* OpenAPI
* gRPC
* GraphQL
* AsyncAPI

## Upcoming Features

- [x] New Relic APM Plugin

- [x] Scaffolder - Templatize your repositories, infrastructure, and more.

- [x] Kubernetes Plugin: Associate your Kubernetes Resources with the Software Catalog
