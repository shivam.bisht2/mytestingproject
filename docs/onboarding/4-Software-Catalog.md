![](img/kind-component.gif)

A `Component` describes a software component.

| **Field** |	**Value** |
|---|---|
| apiVersion |	backstage.io/v1alpha1 |
| kind |	Component |

A Component constitutes a collection of common software assets that can be grouped and viewed together.

Software Components on Backstage comes with the following functionalities:

A software catalog of `kind: Component` comes with the following three out-of-the-box offerings:

- [x] `Overview` Section that includes a link to `View Source` to your VCS
- [x] `Docs` Section with TechDocs support for documentation in  Markdown in your repository
- [x] `GitLab Plugin`

Additional features are available for specific `spec.type` for `kind: Component` which are discussed later in the playbook.

# Standard YAML Structure for `kind: Component`

```yml
apiVersion: backstage.io/v1alpha1
kind: Component
metadata:
  name: [value]
  namespace: [value] 
  description: [value]
  labels: 
    [prefix/name]: [value]
  annotations:
    gitlab.com/project-slug: [value] 
    backstage.io/techdocs-ref: [value] 
    gitlab.com/project-id: [value]
  tags: 
    - [value]
  links: 
    - url: [value]
      title: [value]
      icon: [value]
spec:
  type: [value]
  lifecycle: [value] 
  owner: [value] 
  subcomponentof: [value]
  providesApis: [value]
  consumesApis: [value]
  dependsOn: [value]
```

# Sample Component Catalog Descriptor file

```yml
apiVersion: backstage.io/v1alpha1
kind: Component
metadata:
  name: platform-engineering-portal
  description: |
    William Hill Engineering Portal
  tags:
    - backstage
    - portal
    - william-hill
  links:
    - title: Backstage Platform
      url: https://backstage.io/
      icon: http
  annotations:
    gitlab.com/project-slug: williamhillplc/platform-engineering/portal/platform-engineering-portal
    backstage.io/techdocs-ref: dir:.
    gitlab.com/project-id: '28282364'
    backstage.io/kubernetes-id: "eks01"
spec:
  type: service
  lifecycle: experimental
  owner: portal
```

![](img/component-example.png)

# Understanding the Standard YAML Structure

## metadata.name

* Required field
* Should be unique for `Component` kind throughout backstage if namespace is not defined, else must be unique within the namespace.
* Number of characters: `[1,63]`
* Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

## metadata.namespace

* Optional field
* Preferred if group name of WH Team
* Number of characters: `[1,63]`
* Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

## metadata.labels

* Optional field
* Key-value pairs
* Classify the component
* Reference component to other components
* Used to filter search/queries
* Labels are of format `key: value` where both keys and values are strings.
* Key is a combination of prefix and name of the key separated by `/`

* `Prefix` Rules:
  * lowercase characters only
  * Number of characters: `[1,253]`
  * Backstage keyword prefix `backstage.io/`

* `Name` and `Value` rules:
  * Number of characters: `[1,63]`
  * Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

## metadata.annotations

* Optional field
* Generally used to link external services
* Similar key-value pairs and restrictions as above

As discussed above, we have these out-of-the-box offerings:

![](img/out-of-the-box.png)

To enable these features, we should add the following `metadata.annotations`

1. GitLab Slug: `[username/repository name]` or `[GitLab Group Name]/[Sub-Group Name]/Repository`
2. TechDocs Reference: `url: [absolute URL]` or `dir: [Relative path wrt catalog]`
3. GitLab Project ID: To view GitLab metrics; Contributor's list, Merge Requests status, pipelines, etc.

```yml
metadata:
  annotations:     
    gitlab.com/project-slug: [value]
    backstage.io/techdocs-ref: dir:.
    gitlab.com/project-id: [value]
```

## metadata.tags

* Optional field
* Classify components
* In this context, tags to define the repository
* Single valued strings

## spec.type

In today's world of Microservice architecture, we isolate different software components so that they are built, maintained, and deployed independently.

We have covered the three out-of-the-box offerings for `kind: Component` in the `metadata.annotations` section above. 
Those offerings are true for all values you specify in `spec.type` for `kind: Component`

Backstage allows us to manage the Software Catalog of `kind: Component` in three special categories defined by `spec.type` in the catalog definition.

The following special categories have extra features and support in Backstage.

### `library`: Library - NPM, Java library etc. 

![](img/library.png)

Libraries are one of the well-known and commonly used categories but it does not come with any additional features over the default component features.

### `website`: A website

![](img/website.png)

Additional features over default Components:

- [x] [Dependencies](managing-dependencies.md)
- [x] Security Policies (***Coming Soon***)

### `service`: Backend Service, generally exposing or consuming APIs

![](img/svc.png)

Additional features over default Components

- [x] [Dependencies](managing-dependencies.md)
- [x] Security Policies (***Coming Soon***)
- [x] [Stitching APIs (onboarded in Backstage) with Service catalog](7-Stitch-API-With-Catalog.md)
- [x] [Associating Kubernetes Resources with Service Catalog](9-Kubernetes.md)

## spec.owner

* required field
* Can be any string but should be standardized
* Commonly used values:

- [x] Onboarded Group `metadata.name` 
- [x] Onboarded User `metadata.name`
- [x] Onboarded Email: `fname.lname@williamhill.co.uk`

## spec.lifecycle

* required field
* Can be any string but should be standardized
* Commonly used values:

- [x] `experimental`
- [x] `production`
- [x] `deprecated`