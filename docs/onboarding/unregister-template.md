![](img/unregister-templates.gif)

Unlike other Software Entities in Backstage, Templates cannot be unregistered by following the [standard procedure](unregister-template.md).

To unregister a Software Template, we follow these steps:

* Click on `Search` on Top Left and find your template in registered Backstage items

![](img/del_template_1.png)

* Select the Template you want to delete - double check that it is of `kind: Template`.

![](img/del_template_2.png)

The Template Catalog page opens up.

![](img/del_template_3.png)

* Click on Top Right Icon: it opens up the `Unregister Entity` option on the pop-up

![](img/del_template_4.png)