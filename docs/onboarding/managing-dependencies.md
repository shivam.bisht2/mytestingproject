![](img/managing-dependencies.gif)

A software component of `kind: Component` with `spec.type: service/website` has additional functionalities of attaching dependencies on other `Component`(s) and `Resource`(s) registered on Backstage.

![](img/dependency-1.png)

To define the dependencies, we have to modify the catalog descriptor file.

Doing this is simple, we just have to add the following in the catalog file:

```yml
spec:
  dependsOn: 
    - Component:[metadata.name of a registered Component]
    - ...
    - Resource:[metadata.name of a registered Resource]
    - ...
    ...
```

Backstage currently has a `dependsOn` tag and no `dependencyOf` notion. For more information, please follow this [link](https://github.com/backstage/backstage/issues/6334).

## Example Component Catalog Descriptor with Dependencies

```yml
apiVersion: backstage.io/v1alpha1
kind: Component
metadata:
  name: platform-engineering-portal
  description: |
    Platform Engineering Portal for William Hill built on top of Backstage.io
  tags:
    - react
    - helm
    - gitlab
  links:
    - title: Backstage Platform
      url: https://backstage.io/
      icon: http
  annotations:
    gitlab.com/project-slug: williamhillplc/platform-engineering/portal/platform-engineering-portal
    backstage.io/techdocs-ref: dir:.
    gitlab.com/project-id: '28282364'
    backstage.io/kubernetes-id: "eks01"
spec:
  type: service
  lifecycle: experimental
  owner: portal
  dependsOn: 
    - Component:session-service
    - Component:player-api
```

![](img/dependency-ss.png)