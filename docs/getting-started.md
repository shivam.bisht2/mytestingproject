To onboard any software entity on Backstage, we follow these four simple steps:

1. Define the Software Entity Catalog Descriptor in YAML format
2. Store the catalog file on GitLab 
3. Copy the URL to the catalog file on GitLab - `https://www.gitlab.com/[project-slug]/-/blob/[branch name]/[path to catalog file]`
4. Register the Software Entity with its descriptor file in Backstage 


## 1. Define the Software Entity Catalog Descriptor in YAML format

Your Catalog Descriptor file should define the a `kind: Component` and your `spec.type` defines the type of component.
The supported options for `spec.type` are:

* `service`
* `website`
* `library`

Each of these options will enable/disable certain plugins automatically, as described in [here](./onboarding/4-Software-Catalog/#spectype)

An example `service` could be defined as in this YAML:


```yml
apiVersion: backstage.io/v1alpha1
kind: Component
metadata:
  name: my-service
  description: |
    My service...
  tags:
    - some-service
  links:
    - title: Example Link
      url: https://go2.williamhill.com/
      icon: http
  annotations:
    gitlab.com/project-slug: #Gitlab path, eg. williamhillplc/channel/some/repo
    gitlab.com/project-id: # Enter the Gitlab project ID (int)
spec:
  type: service
  lifecycle: experimental
  owner: my-channel
```

## 2. Store the catalog file on GitLab

This file has to be named 'catalog-info.yaml' (note: this will be later used for auto-discovery), and be stored at the root of your repository.

## 3. Copy the URL to the catalog file 

Eg, on GitLab - `https://www.gitlab.com/[project-slug]/-/blob/main/catalog-info.yaml`

## 4. Register the Software Entity with its descriptor file in Backstage 

See [here](./onboarding/10-import-catalog.md) for a more in-depth component registration.


* Open Backstage on the Browser. Click on the `Create` Button on the Sidebar.

![](./onboarding/img/register-1.png)

* Click on `Register Existing Component` on the top right.

![](./onboarding/img/register-2.png)

* A Registration page for the software catalog opens up.

![](./onboarding/img/register-3.png)

* Paste your URL and click on `Analyze`. Backstage validates the semantics and schema of the catalog descriptor file.

Post successful analysis, click on `Import` to register the catalog into Backstage.

![](./onboarding/img/register-4.png)

![](./onboarding/img/register-5.png)

Once registered, you can view your Entity at Backstage.

![](./onboarding/img/registered-1.png)

Click on the registered catalog to view all supported features in action.

![](./onboarding/img/registered-2.png)



