# New Relic Queries and Apps for Platform Engineering Poral 

[https://onenr.io/0a7j9nZnAQO](https://onenr.io/0a7j9nZnAQO) Short link for the query builder
### Queries

-   For fetching unique usernames logged in :  
      
**SELECT Uniques(userName) FROM PageAction WHERE actionName='userSession' SINCE 24 hour ago WHERE appId = 1095743460 LIMIT 1000**  
Permalink: [https://onenr.io/0dOQM8GrxjG](https://onenr.io/0dOQM8GrxjG)
-   For getting User Information

**SELECT (userName ,fullName, profilePhoto) FROM PageAction WHERE actionName='userSession' SINCE 24 hour ago WHERE appId = 1095743460 LIMIT 1000**

Permalink: [https://onenr.io/0oqQanW95w1](https://onenr.io/0oqQanW95w1)  

### Apps

-   **Geography** - gives details on users location

Permalink: [https://onenr.io/0a2wd8A8GwE](https://onenr.io/0a2wd8A8GwE)

-   **Datalyzer** -It's a tool to filter data and actions

Permalink: [https://onenr.io/0znQxnZdpRV](https://onenr.io/0znQxnZdpRV)

-   **Site Analyzer** - A good reporting tool to check web app health and performance

Permalink: [https://onenr.io/0eqwyqgrewn](https://onenr.io/0eqwyqgrewn)