## Import API to Backstage

* 4 supported types of Kind: API

1. OpenAPI (Swagger)
2. AsyncAPI
3. gRPC
4. GraphQL

### Standard API Catalog format

```yml
apiVersion: backstage.io/v1alpha1
kind: API
metadata:
  name: [value]
  namespace: [value]
  description: [value]
  labels: 
    [key]: [value]
  tags: 
    - [value]
spec:
  type: [value]
  lifecycle: [value]
  owner: [value]
  system: [value]
  definition:
    [API Specification]
```

#### metadata.name

* Required field
* Should be unique for `API` kind throughout backstage if namespace is not defined, else must be unique within the namespace.
* Number of characters: `[1,63]`
* Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

#### metadata.namespace

* Optional field
* Preferred if group name of WH Team
* Number of characters: `[1,63]`
* Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

#### metadata.labels

* Optional field
* Key-value pairs
* Classify the component
* Reference component to other components
* Used to filter search/queries
* Labels are of format `key: value` where both keys and values are strings.
* Key is a combination of prefix and name of the key separated by `/`

* `Prefix` Rules:
  * lowercase characters only
  * Number of characters: `[1,253]`
  * Backstage keyword prefix `backstage.io/`

* `Name` and `Value` rules:
  * Number of characters: `[1,63]`
  * Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

#### metadata.tags

* Optional field
* Classify components
* In this context, tags to define the repository
* Single valued strings

#### spec.type

* required field - name of the API format
* Supported values
  * openapi
  * grpc
  * asyncapi
  * graphql

#### spec.owner

* required field
* Can be any string but should be standardized
* Commonly used values
  * Onboarded Group `group name`
  * Onboarded Team `team name` 
  * Onboarded User `user:username`
  * Onboarded Email: `fname.lname@williamhill.co.uk`

#### spec.lifecycle

* required field
* Can be any string but should be standardized
* Commonly used values
  * `experimental`
  * `production`
  * `deprecated`

#### spec.definition

* required field - the definition of the API
* Can take following formats:

1. Inline definition

```yml
definition: |
  ...
```

2. $text - rended API definition from generic file

```yml
definition: 
  $text: [relative path to file or URL]
```

3. $json - parse and validate definition written in json file

```yml
definition: 
  $json: [relative path to file or URL]
```

4. $yaml - parse and validate definition written in yaml file

```yml
definition: 
  $yaml: [relative path to file or URL]
```

### Example API-Docs catalog file

* Software Catalog Example to Import API

```yml
apiVersion: backstage.io/v1alpha1
kind: API
metadata:
  name: Inventory-Management-With-URL-Specification
  description: WH Inventory Catalog
spec:
  type: openapi
  lifecycle: production
  owner: nishkarsh.raj@williamhill.co.uk
  definition:
    "$text" : "https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/catalog/sample-api.yaml"
```

* `app-config.yaml` additions for reading lists to enable reading 3rd party sites like Swagger etc.

```yml
backend:
  baseUrl: ...
  reading:
    allow:
      - host: example.com
      - host: '*.examples.org'
```

#### Case Study: Importing an API by defining it within the Catalog (Not linking 3rd party URL)

https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/catalog/sample-api-catalog.yaml

### References

https://backstage.io/docs/features/software-catalog/descriptor-format#substitutions-in-the-descriptor-format
https://backstage.io/docs/features/software-catalog/descriptor-format#kind-api
