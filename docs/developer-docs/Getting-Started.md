### Spinning Up Backstage Application hosted on SCM

* Clone the application from Source Control

```sh
git clone https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal
cd platform-engineering-portal/portal-app
```

* Store your secrets and credentials as environment variables on your machine

Open your terminal and type the following commands with required values replacing the placeholders.

```sh
# GitLab
## Read Only Application Token - https://gitlab.com/-/profile/applications
## Call back URL - `http://{APP_FQDN}/api/auth/gitlab/handler/frame`  # Default APP_FQDN: http://localhost:7000
export AUTH_GITLAB_CLIENT_ID=[value]
export AUTH_GITLAB_CLIENT_SECRET=[value]

## Full Access Personal Access Token - https://gitlab.com/-/profile/personal_access_tokens
export GITLAB_TOKEN=[value]

# Postgres
export POSTGRES_HOST=[value]
export POSTGRES_PORT=[value]
export POSTGRES_USERNAME=[value]
export POSTGRES_PASSWORD=[value]

# Domain
export DOMAIN_FRONTEND=[value] # default: http://localhost:7000
export DOMAIN_BACKEND=[value] # default: http://localhost:3000
```

If you want to automate the above task on a MacBook, copy the lines into `~/.zshrc` and execute it with `source ~/.zshrc`

*  Start your backend and frontend servers concurrently

```node
yarn install
yarn dev
```

* Move to http://localhost:3000 for accessing the User Interface.
