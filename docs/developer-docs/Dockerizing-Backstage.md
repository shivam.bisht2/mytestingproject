## :whale2: Dockerizing Backstage

### Pre-Requisities

1. Docker Engine 

[Docker Engine](https://docs.docker.com/engine/install/) installed on your machine

2. Postgres Instance

Follow this [guide](Postgres-Setup.md) to setup Postgres.

3. Environment file

* Create a environment file or use [existing template](portal-app/env.list)
* Make sure that you save the environment file with extension `.list` - the playbook assumes that you name your file as `env.list` going forward.

### Containerize your backstage application from the scratch 

This section helps us create a Docker Image for Backstage instance, based on this [Dockerfile](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/portal-app/packages/backend/Dockerfile)

* Docker Context - Backstage application

```sh
cd [repository root / application name]
```

**Note:** If you have downloaded this repository, do the following:

```sh
cd platform-engineering-portal/portal-app/
```

```sh
## Set default domain - overridden with environment values in env.list
export DOMAIN_FRONTEND=http://localhost:3000
export DOMAIN_BACKEND=http://localhost:7000

## Build the project locally
yarn install --frozen-lockfile
yarn tsc
yarn build

## Dockerize the project
docker build -t [image-name] -f packages/backend/Dockerfile .
docker run --env-file ./env.list -it -p 3000:7000 -p 7000:7000 [image-name]
```

### FAQs

#### 1) While launching the docker container, the login page shows up but on clicking login, it shows a failed pop-up. How to resolve this issue?

GitLab login is based on tokens mentioned [here](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/portal-app/app-config.yaml#L66)

Make sure when you create these [tokens](https://gitlab.com/-/profile/applications) - The callback URL must be the following

```
http://{APP_FQDN}:{APP_BACKEND_PORT}/api/auth/gitlab/handler/frame
```
