## Adding Support for PlantUML plugin in TechDocs

TechDocs supports PlantUML within its out-of-the-box offerings.

### Install and set plantUML in path in Docker image of Backstage

```Dockerfile
RUN curl -o plantuml.jar -L http://sourceforge.net/projects/plantuml/files/plantuml.1.2021.4.jar/download && echo "be498123d20eaea95a94b174d770ef94adfdca18  plantuml.jar" | sha1sum -c - && mv plantuml.jar /opt/plantuml.jar
RUN echo '#!/bin/sh\n\njava -jar '/opt/plantuml.jar' ${@}' >> /usr/local/bin/plantuml
RUN chmod 755 /usr/local/bin/plantuml
```

## Test the plugin

```plantuml format="png" classes="uml myDiagram" alt="My super diagram placeholder" title="My super diagram" width="300px" height="300px"
  Goofy ->  MickeyMouse: calls
  Goofy <- MickeyMouse: responds
```

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```

### References

* Original Plugin for Markdown x PlantUML

https://github.com/mikitex70/plantuml-markdown
https://pypi.org/project/plantuml-markdown/

* Backstage Support for the plugin

https://github.com/backstage/backstage/issues/2068
https://github.com/backstage/mkdocs-techdocs-core#mkdocs-plugins-and-extensions
https://backstage.io/docs/features/techdocs/troubleshooting#plantuml-with-svg_object-doesnt-render
