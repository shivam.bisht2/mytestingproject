## Creating a Postgres instance to act as database for our Backstage Application

### Launching a Postgres Docker Container (Insecure Method)

```
docker run -e POSTGRES_PASSWORD=[password] -p [port]:5432 postgres
```

Your credentials would be the following:

* Host: http://127.0.0.1
* Port: Value you mention in above command
* Username: postgres
* Password: Value you mention in above command

* Note: If you use a docker container for database as well as the backstage application, they must be on the same [Docker Network](https://docs.docker.com/network/)

### Creating an AWS RDS Instance (Recommended Method)

* Follow this simple [guide](https://towardsdatascience.com/how-to-set-up-a-postgresql-database-on-amazon-rds-64e8d144179e) to setup AWS RDS Instance for setting up Postgres.