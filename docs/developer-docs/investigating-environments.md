# Identifying Technical Capabilities for Multiple Environments 

## Table of Contents

* The need to introduce the concept of environment into Backstage
    * Understanding William Hill’s notion of environments
    * Different teams – different notions of environments
    * Same Component – n catalog files for n environments?
*  How we can scope the services into different environments with current out-of-the-box YAML definition of Backstage Service Catalog
*  Understanding how Spotify manages the problem of handling multiple environments
* Plugins incorporated by CPE in Backstage – Blockers and solutions on scoping:
    * GitLab Repository Catalog
    * API Catalogs
    * Kubernetes Resources
    * New Relic
    * Pager Duty
* Proposed UI/UX Changes once we crystallize the policies of how we are going to onboard the
services and scope them into different environments

## Need for Environments in Backstage

Any IT organization follows the standard practice of keeping multiple environments for different purposes:

* Dev – Developer playground
* Test (Unit/Integration/Functional) – QA testing the developed functionality
* OAT – QA testing the non-functional aspects like load, security, regression etc.
* UAT – Trusted end-users getting preview access to give feedback
* Non-Prod (Staging) - Replicates the production environment to emulate end-user
experience
*  Prod – Production environment for end-users

William Hill is a global online gambling organization that has over 10k employees distributed into many teams and sub-teams. Each team has its different notions and requirements of environments.

In our aim to adopt Backstage as William Hill’s primary development portal across teams, we have realized that the current support for onboarding the same service in different environments is limited and highly contrived.

## How to scope a catalog into environments with current Backstage offerings

### Namespace Approach

In the current offering, we can scope a Service Catalog item into different environments, per se, by declaring them in a certain namespace.

Backstage has following documentation around [namespaces](https://backstage.io/docs/features/software-catalog/descriptor-format#namespace-optional):

* A service catalog must be unique within a namespace
* If no namespace is explicitly defined, a service is then mapped in the default namespace
* If a namespace is defined, the name of the service catalog must be unique within the said namespace

The reserved word has a meaning for grouping and we can formalize our policy to use it to group each catalog item into different environments.

**Pros:**

* Grouping in a different environment would be achieved
* Owner would be `namespace/[owner mail/team group]`
* Ability to filter onboard catalogs per owner/namespace on Backstage portal

**Cons:**

* Complex definition of standard number of accepted environments (in this case, namespace) and all the teams would have to change the existing environment names to follow the standard
* Maintaining N number of catalogs for the same Component with just a single line of change defining the namespace
* Backstage upstream project is planning to use namespaces for the purpose of setting boundaries for security and authorization - [discussion](https://discord.com/channels/687207715902193673/687235481154617364/878010440578392134)

## Current Features Onboarded in William Hill’s Backstage

### GitLab Repository Catalog

The notion of environment in Backstage is not similar to products/services - it is the branching strategies.

It is a standard practice to never make changes directly to the main branch of the GitLab repository.

William Hill enforces the policy to create MR onto the main branch that undergoes due diligence review and requires all checks and pipelines to pass successfully.

Backstage onboards a Service Catalog item defined by a YAML kept on Source control. William Hill primarily uses GitLab and the URL for storing catalog item is of the following format:

`https://gitlab.com/[organization]/[group and subgroups]/[repository name]/-/blob/[branch name]/[path to file]/catalog-info.yaml`

* If a catalog is onboarded always from the default branch
  * A developer making changes to the catalog can never visualize the consequences of the changes he/she has made to the catalog definition until the MR is completed and the catalog is synchronized with the Backstage portal
* If a catalog is allowed to be onboarded from non-mainstream branch:
  * Backstage is unable to distinguish between what branch the catalog item is getting onboarded, for it – all the sources are same and only the definition written within the pointed source matters
  * If the developer wants to test the catalog before merging with the MR, he can onboard the catalog by changing the name/namespace to make it unique in Backstage

#### Proposed Solutions

* Namespace used as Branch name for uniqueness of repository per branch

  * **Drawback:** Owned components on the homepage show the services you and your team own. If a namespace is used, the owner is namespace/owner and currently
that would lead to the service not showing in the Owned section.
  * **Proof of Concept:** The GitLab Backstage repository of William Hill is onboarded
onto Backstage from the [main](https://backstage.sit.platform-eng.aws-ew1.dv.williamhill.plc/catalog/default/component/platform-engineering-portal) branch and a `poc-branching` branch that has an extra definition of `metadata.namespace`

* **RFC** - Create a notion of branch keyword to onboard GitLab repositories

### New Relic Plugin

The community New Relic plugin currently incorporated in William Hill’s Backstage instance has limited features:
* Supports only APM (Application Performance Monitoring) - RFC / Ticket raise needed
* Currently supports a single account (Environment) for a specific team via the NRRA (New Relic REST API) Key
* No support for pagination and filtering – Ticket raised – RFC to upstream needed

The documentation for onboarding the current plugin can be found [here](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/docs/new-relic-plugin.md)

#### Proposed changes to be made on top of the existing plugin

* Onboard different teams onto WH Backstage and get NRRA keys for different environments they maintain
* Create (N x M) proxies in the Backstage configuration for N teams x M environments onboarded
* Remove the current support for a New Relic Sidebar in the Backstage application
* Add a Top Bar Entity to Backstage Components (Service, Websites and Libraries) for the linked New Relic components
* The Top Bar Entity when clicked has the following features:
  * Shows the APM in default account (environment) linked with the team that owns the Service Catalog in Backstage - can be decided internally
    * For this, the New Relic plugin must make a REST Call, for the specific team (owner of service catalog) and the default environment mutually decided, to the proxy defined within the Backstage configuration
  * This Top Bar Entity must have dropdown support to switch between different New Relic Account / WH defined environments
    * When a specific environment is selected, a REST Call for that proxy must be made and the APMs should be shown on the UI

### Open API Catalogs

#### Proof of Concept Around scoping API Catalogs into different environments

To demonstrate the notion of using namespaces for scoping service catalog into different environments – thereby meaning, maintain N catalog for N different environments, we did a proof concept to onboard a single Inventory Management API in three different environments:

* Dev
* Test
* Prod

The YAML definition for the API Specification can be found [here](https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/tree/main/api/environment-poc)

The onboarded APIs appear on the Backstage portal like this:

![](img/poc1.png)

**Comments:**

* The API onboarded is a single catalog item with the same name – inventory
* The name appearing on the Backstage portal is prefixed by the namespace to make the onboarded service unique
* The owner of the service is prefixed by the namespace

By opening any of the above API, we see the following Overview page:

![](img/poc2.png)

* The name on the top of the page says “Catalog Item Name is Namespace” that could be our notion of “Service Catalog Item in an Environment”

### Kubernetes Plugin 

One of the Core Features that Backstage offers is the Kubernetes plugin.

William Hill is currently on AWS Cloud and uses managed EKS (Elastic Kubernetes Services) Clusters.

Given the fact that both Kubernetes resources and Backstage's Service Catalog items are defined in YAML format and have a common parameter `namespace` in the definition, one would assume that it could be used to segregate the WH EKS Clusters into isolated environments but as touched upon earlier

> Backstage notion of namespaces in the longer run would be more based on Authorization and Identity rather than segregating resources into namespace.

Furthermore, the Kubernetes Plugin is not a standalone feature like the `APIs` which we can see on the sidebar

![](img/sidebar.png)

Unlike APIs, where we can onboard different types of APIs with different names and definitions, we cannot onboard Kubernetes Clusters and glance overview of them in the UI via the out-of-the-box offerings.

The current Backstage support is to onboard Kubernetes clusters as catalog items to create a domain of searching resources.

```yml
kubernetes:
  objectTypes: ["configmaps","deployments","horizontalpodautoscalers","ingresses","pods","replicasets","services"]
  serviceLocatorMethod:
    type: 'multiTenant'
  clusterLocatorMethods:
    - type: 'config'
      clusters:
        - url: [url of the hosted kubeconfig]
          name: [cluster-name]
          authProvider: ['aws','google','serviceAccount']
          skipTLSVerify: [true/false]
          serviceAccountToken: []
        - ...
```

![](img/kube-architecture.png)

The Kubernetes plugin currently supports linking a specific resource to `kind: Component` with the following limitations -

* [Limitation 1](https://backstage.io/docs/features/kubernetes/configuration#servicelocatormethod): We can specify and link a Kubernetes resource to Service Catalog item but currently no support is added to specify the cluster the resource lies in. The current support is only `multiTenant` and the linked resource is searched in all the onboarded clusters irrespective of the fact that the resource lies in that group of not.

This is redundant and also might cause security issues since Backstage would search for a specific WH's team resource in all the other teams clusters that are onboarded.

* Limitation 2: No support for custom AWS Authorization like GKE

* Limitation 3: No segregation of Environment support out-of-the-box

* [Limitation 4](https://backstage.io/blog/2021/01/12/new-backstage-feature-kubernetes-for-service-owners#rethinking-the-developer-experience): Resource level view instead of a cluster level view. This according to Backstage is one of the best things this plugin offers because it links resource to a specific component and makes Backstage more of a `Developer` portal

## Proposed Environment Strategy in Backstage

The document above covered two possible environment strategy:

**1. Using Namespaces** 

N service catalog copies for N different environments

This method should not be used because the usage of `namespace` in Backstage going forward would be to set authorization boundary and Identity resolution.

**2. William Hill maintains N Backstage Instance for N environments**

This will work in theory but since there are no standard notion of environment across different teams in William Hill, we cannot use this.

Even if standardization is achieved across William Hill, maintaing N numbers of environments would require huge cost in terms on capital, resources and effort.

### Proposed Solution

Use a single Software Catalog item to define a resource/service across different environments - resolve the environment internally basis the selection on the User Interface exposed to the end-users.

This proposed solution assumes that we define the environments a catalog can exist in.

The following environments can be used (Open to suggestions/additions):

* SIT
* OAT
* UAT
* Non-Prod
* PROD

Note: There are services in Backstage that are environment independent like GitLab and inhouse CI/CD which can depend on different factors like `branches` etc.

### UI/UX proposed

Link to Figma [wireframes](https://www.figma.com/proto/w0k61EaMR3F37mVGopw0zV/WH-UI-UX?node-id=24%3A2&scaling=min-zoom&page-id=0%3A1&starting-point-node-id=24%3A2)

### Next Steps

* Discussion with Spotify team to understand their Ways of Working
* [Questionnaire](https://conf.willhillatlas.com/display/PEng/Questionnaire+for+adopters) with Head of Engineering of William Hill teams to understand their Ways of Working and requirements.
* Refine the proposed solution with feedback and suggestions taken from the above two discussions
* Discussion with Head of Engineering of different William Hill teams to discuss the proposed solution

