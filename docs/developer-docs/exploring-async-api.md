# AsyncAPI

Today's world is of microservice architecture - different distributed and loosely coupled components are built, shipped and maintained independently by different teams. These independent components can be created by any language or framework. For two components to interact, the owners should mutually decide boundaries of accepted request and response and document them in an API.

All teams must explicitly define any information exchanged across these boundaries. For example, all teams must maintain broker configurations, topics, and event formats in a central place.

> AsyncAPI is an open-source initiative that provides both a specification to describe and document your asynchronous applications in a machine-readable format and tooling (such as code generators) to make life easier for developers tasked with implementing them.

AsyncAPI is built on the foundation of OpenAPI specification.

An AsyncAPI document is a file that defines and annotates the different components of a specific Event-Driven Application. The file format must be JSON or YAML; however, only the subset of YAML that matches the JSON capabilities is allowed.

## AysncAPI Architecture

1. Applications: (N applications - N AsyncAPI documents - 1 for each)

> An application is any kind of computer program or a group of them. It MUST be a producer, a consumer or both. An application MAY be a microservice, IoT device (sensor), mainframe process, etc. An application MAY be written in any number of different programming languages as long as they support the selected protocol. An application MUST also use a protocol supported by the server in order to connect and exchange messages.

2. Servers/ Message Brokers - Capable to send and receive information

- [x] [MQTT](https://mosquitto.org/) - MosquitoMQ
- [x] [AQMT](https://www.rabbitmq.com/tutorials/amqp-concepts.html) - RabbitMQ
- [x] [Kafka](https://kafka.apache.org/)

3. Operation - Mapping b/w Application and Server

- [x] Publish (A -> S)
- [x] Subscribe (A <- S)

4. Message - Exchange of information over operations - can be one or more

5. Channel - A channel is an addressable component made available by the server for the organization of messages.
Operations are bound to a particular channel in the server, along with the messages they exchange
A server is base host but channels are concrete definitions of accepted messages that can be published or consumed. A channel is a sort of an interface (a Queue basically) between the applications and the server.

6. Event - triggers interaction b/w Different components. Described within the message along with associated payload schema

### Standard YAML Structure of AsyncAPI

```yml
asyncapi: 2.0.0

info:
  title: [value]
  version:  [value]

description: |
  [value]

license:
  name: [name of license]
  url: [url to license definition]

servers:
  production:
    url: [server host URL]
    protocol: [name of protocol]
    description: [value]

channels:
  [name of the channel]:
    [publish/subscribe]:
      operationId: [value]
      message:
        $ref : '#/components/messages/[name of the message]'

components:
  messages:
    [message name]:
      name: [message name]
      title: [title]
      summary: [definition]
      contentType: [value] # Example: application/json
      payload:
        $ref: '#/components/schemas/[schema name]'

  schemas:
    [schemaName]:
      type: object
      properties:
        firstName:
          type: string
          description: "foo"
        lastName:
          type: string
          description: "bar"
        email:
          type: string
          format: email
          description: "baz"
        createdAt:
          type: string
          format: date-time
          description: "foo"
```

## Sample Onboarding Architecture to demonstrate AsyncAPI

Generic onboarding architecture trigger multiple independent microservices based on `SignUp`.

A simplistic is depicted below:

![](https://miro.medium.com/max/1400/1*pNvIViY4x5vZhOlnM8dqUw.png)

Here, two applications are triggered by `UserSignUp` Event.

1. Account Service: Subscribed to the Message broker containing queue of `UserSignUp` events. Once an event is encountered, an account is created and registered in the database.

2. Email Service: Publishes Emails to the Message Queue. Whenever a new user signs up, he receives the generic Welcome Email.

Since the complex architecture is out-of-scope in our discussion, we would limit our scope by stating the following:

> An Event Driven Architecture requires N number of AsyncAPI definitions for N applications/microservices because different applications have different behaviour wrt. to the message broker (Servers) - publish, consume, or both.

**For Backstage adopters, we propose that different users combine these multiple API definitions of a single event-driven architecture into a single `spec.system` for organization of APIs**

### Defining The Account Service

```yml
asyncapi: 2.0.0
info:
  title: Account Service
  version: '1.0.0'
  description: |
    This service is responsible for managing user accounts in the system.
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0

servers:
  production:
    url: mqtt://test.mosquitto.org
    protocol: mqtt
    description: Test MQTT broker

channels:
  user/signedup:
    subscribe:
      operationId: emitUserSignUpEvent
      message:
        $ref : '#/components/messages/UserSignedUp'

components:
  messages:
    UserSignedUp:
      name: userSignedUp
      title: User signed up event
      summary: Inform about a new user registration in the system
      contentType: application/json
      payload:
        $ref: '#/components/schemas/userSignedUpPayload'

  schemas:
    userSignedUpPayload:
      type: object
      properties:
        firstName:
          type: string
          description: "First name of the person registered"
        lastName:
          type: string
          description: "Last name of the person registered"
        email:
          type: string
          format: email
          description: "Email of the registered person"
        createdAt:
          type: string
          format: date-time  
```

### Defining the Email Service 

```yml
asyncapi: 2.0.0
info:
  title: Email Service
  version: '1.0.0'
  description: |
    This service is responsible for sending out emails upon certain events
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0

servers:
  production:
    url: mqtt://test.mosquitto.org
    protocol: mqtt
    description: Test MQTT broker

channels:
  user/signedup:
    publish:
      operationId: onUserSignUp
      message:
        $ref : '#/components/messages/UserSignedUp'

components:
  messages:
    UserSignedUp:
      name: userSignedUp
      title: User signed up event
      summary: Inform about a new user registration in the system
      contentType: application/json
      payload:
        $ref: '#/components/schemas/userSignedUpPayload'

  schemas:
    userSignedUpPayload:
      type: object
      properties:
        firstName:
          type: string
          description: "First name of the person registered"
        lastName:
          type: string
          description: "Last name of the person registered"
        email:
          type: string
          format: email
          description: "Email of the registered person"
        createdAt:
          type: string
          format: date-time 
```

### Defining the Backstage Catalog Item

```yml
---
apiVersion: backstage.io/v1alpha1
kind: API
metadata:
  name: account-service
  description: Account Service AsyncAPI definition
  tags:
    - asyncapi
    - event-driven-architecture
    - message-brokers
  links:
    - title: Source
      url: https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/tree/main/api/async-api-example
      icon: http
spec:
  type: asyncapi
  lifecycle: production
  system: backstage-sample
  owner: portal
  definition:
    "$text" : "https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/api/async-api-example/account-service.yaml" 
---
apiVersion: backstage.io/v1alpha1
kind: API
metadata:
  name: email-service
  description: Email Service AsyncAPI definition
  tags:
    - asyncapi
    - event-driven-architecture
    - message-brokers
  links:
    - title: Source
      url: https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/tree/main/api/async-api-example
      icon: http
spec:
  type: asyncapi
  lifecycle: production
  owner: portal
  system: backstage-sample
  definition:
    "$text" : "https://gitlab.com/williamhillplc/platform-engineering/portal/platform-engineering-portal/-/blob/main/api/async-api-example/email-service.yaml" 
```

### Viewing the APIs in Backstage

- [x] [Account Service](https://backstage.sit.platform-eng.aws-ew1.dv.williamhill.plc/catalog/default/api/account-service/)
- [x] [Email Service](https://backstage.sit.platform-eng.aws-ew1.dv.williamhill.plc/catalog/default/api/email-service)

## References and Further Study

https://www.asyncapi.com/

https://medium.com/event-driven-utopia/understanding-asyncapis-with-a-practical-example-ee2b4be221d8

https://github.com/asyncapi/generator
