## Integrating the New Relic APM Plugin with Backstage Application

To install New Relic on the Backstage Application, following changes are required:

* Import the community made NPM package of New Relic Plugin in the Backstage application and add a route path to it.

`packages/app/src/App.tsx`

```ts
import { NewRelicPage } from '@backstage/plugin-newrelic';
...
    <Route path="/newrelic" element={<NewRelicPage />} />
```



* `[OPTIONAL]` Add a sidebar component that is mapped to New Relic Plugin

`packages/app/src/components/Root/Root.tsx`

```ts
<SidebarItem icon={MapIcon} to="newrelic" text="New Relic" />
```

* Create a Proxy to New Relic APIs and pass the **New Relic Rest API** Keys as environment variables 

`app-config.yaml`

```yml
proxy:
  '/newrelic/apm/api':
      target: https://api.newrelic.com/v2
      headers:
        X-Api-Key: ${NR_NRRA_Key}
```

* Create an environment variable to the container within EKS in the deployment file.

`helm/templates/deployment.yaml`

```yml
env:
  - name: NR_NRRA_Key
    value: {{ .Values.env.newrelic }}
```

* Pass the KMS Encrypted value in the Helm values file

`helm/values.yaml`

```yaml
env:
  newrelic: "value"
```

### References

https://www.npmjs.com/package/@backstage/plugin-newrelic