## Standard Catalog for Registering a GitLab Repository for William Hill

We define a repository to Backstage as a `Component` kind defined by YAML file.

`Component` is a general kind that can be used to define API, Documentation, Libraries etc. but the following playbook defines its usage to link a Repository with Backstage.

While you can name your catalog anything you want, it is recommended to store this catalog as `catalog-info.yaml` at the root of your repository.

* Root fields:
  1. apiVersion: required field
  2. kind: required field
  3. metadata: required field
  4. spec: varies

### Standard YAML Structure

```yml
apiVersion: backstage.io/v1alpha1
kind: Component
metadata:
  name: [value]
  namespace: [value] 
  description: [value]
  labels: 
    [prefix/name]: [value]
  annotations:
    gitlab.com/project-slug: [value] 
    backstage.io/techdocs-ref: [value] 
    gitlab.com/project-id: [value]
  tags: 
    - [value]
  links: 
    - url: [value]
      title: [value]
      icon: [value]
spec:
  type: [value]
  lifecycle: [value] 
  owner: [value] 
```

#### metadata.name

* Required field
* Should be unique for `Component` kind throughout backstage if namespace is not defined, else must be unique within the namespace.
* Number of characters: `[1,63]`
* Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

#### metadata.namespace

* Optional field
* Preferred if group name of WH Team
* Number of characters: `[1,63]`
* Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

#### metadata.labels

* Optional field
* Key-value pairs
* Classify the component
* Reference component to other components
* Used to filter search/queries
* Labels are of format `key: value` where both keys and values are strings.
* Key is a combination of prefix and name of the key separated by `/`

* `Prefix` Rules:
  * lowercase characters only
  * Number of characters: `[1,253]`
  * Backstage keyword prefix `backstage.io/`

* `Name` and `Value` rules:
  * Number of characters: `[1,63]`
  * Accepted characters `[ [a,z], [A-Z], [0-9], '.', '-', '_' ]`

#### metadata.annotations

* Optional field
* Generally used to link external services
* Similar key-value pairs and restrictions as above

**Proposed Annotations to use for repository**

1. GitLab Slug: `[username/repository name]` or `[GitLab Group Name]/[Sub-Group Name]/Repository`
2. TechDocs Reference: `url: [absolute URL]` or `dir: [Relative path wrt catalog]`
3. GitLab Project ID: It's optional to use but if you want to see GitLab metrics like last 10 MR, 10 Pipelines, then it's mandatory to use.

#### metadata.tags

* Optional field
* Classify components
* In this context, tags to define the repository
* Single valued strings

#### spec.type

* required field
* Can be any string but should be standardized
* Commonly used values

  * `website`: A website
  * `service`: Backend Service
  * `library`: Library 

#### spec.owner

* required field
* Can be any string but should be standardized
* Commonly used values
  * Onboarded Group `group name`
  * Onboarded Team `team name` 
  * Onboarded User `user:username`
  * Onboarded Email: `fname.lname@williamhill.co.uk`

#### spec.lifecycle

* required field
* Can be any string but should be standardized
* Commonly used values
  * `experimental`
  * `production`
  * `deprecated`

### Example: A sample Repository Catalog file

```yml
apiVersion: backstage.io/v1alpha1
kind: Component
metadata:
  name: platform-engineering-portal
  description: |
    Platform Engineering Portal for William Hill built on top of Backstage.io
  tags:
    - react
    - helm
    - gitlab
  links:
    - title: Backstage Platform
      url: https://backstage.io/
      icon: http
  annotations:
    gitlab.com/project-slug: williamhillplc/platform-engineering/portal/platform-engineering-portal
    backstage.io/techdocs-ref: dir:.
    gitlab.com/project-id: '28282364'
spec:
  type: service
  lifecycle: experimental
  owner: portal
```

### Reference 

https://backstage.io/docs/features/software-catalog/descriptor-format
