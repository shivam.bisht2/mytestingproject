## PagerDuty Plugin

The PagerDuty plugin is a frontend plugin developed by spotify.

### PagerDuty + Backstage Integration Benefits

- Display relevant PagerDuty information about an entity within Backstage, such as the escalation policy or if there are any active incidents.
- Trigger an incident to the currently on-call responder(s) for a service
- Email link, and view contact information for staff on call.

#### Requirements

PagerDuty Admin role in order to generate the necessary authorizations, such as the API token.

#### Integrating With a PagerDuty Service

There are two ways to add an integration to a service

- Adding your integration to an existing service.
- Creating a new service for your integration.

#### Installation

1. Installing the front-end plugin

```
yarn add @backstage/plugin-pagerduty
```

2. Setup a new proxy endpoint for the PagerDuty API. The PAGERDUTY_TOKEN environment variable will be used to specify a secret access token required to access the PagerDuty API.

```
# app-config.yaml
proxy:
  '/pagerduty':
    target: https://api.pagerduty.com
    headers:
      Authorization: Token token=${PAGERDUTY_TOKEN}
```

3. An entity is connected to a PagerDuty service by adding a pagerduty.com/integration-key annotation to the entity’s catalog-info.yaml file.The integration key can be retrieved from the Integrations tab of the service in the PagerDuty service directory.

```
annotations:
  pagerduty.com/integration-key: [INTEGRATION_KEY]
```

#### Features Overview

![](img/email-link-pagerduty-plugin-img-1.png)

![](img/open-clients-pagerduty-lugin-img-2.png)

![](img/trigger-pager-duty.png)

![](img/pagerduty-plugin-img-1.png)

### References

https://www.npmjs.com/package/@backstage/plugin-pagerduty
