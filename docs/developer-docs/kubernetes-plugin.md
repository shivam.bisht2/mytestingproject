## Backstage Kubernetes Plugin

1. Made for service owners - caters need of service owner rather than cluster admin -> Ownership assigned to each deployment/service
2. Services at glance - See all services in a single pane
3. CloudNative - cloud agnostic and supports multi-cloud
4. Unified UI - Common UI for all cloud platforms and on-premises K8s clusters

### The first release blog and future roadmap

https://backstage.io/blog/2021/01/12/new-backstage-feature-kubernetes-for-service-owners

* Supports all cloud platforms and managed kubernetes providers.

#### Missing link b/w Kubernetes and Services

* Service Catalog 
* How does it links auth though - third party specific / kubeconfig?? 

![](https://backstage.io/blog/assets/21-01-12/backstage-k8s-2-deployments.png)

#### No more context-switching

* Catalog onboards services rather than clusters - no need to switch context of kubectl to see specific service details

#### Automatic Error reporting

![](https://backstage.io/blog/assets/21-01-12/backstage-k8s-3-error-reporting.png)

#### Auto-scaling limits at a glance

* Shows the loads and when autoscaling (up or down) would take place

#### Cloud Agnostic

* Backstage talks directly with Kubernetes API (kubeconfig) rather than provider

#### Rethinking Developer Experience

* By tying Kubernetes resources with a specific catalog, we return control to developers
* Developers are only bothered if their software works and serves it's purpose to end-users and not where or how the cluster is deployed and his/her services are managed!

#### Current offering (as per blog in January 2021)

* Pods
* Deployments
* Replica Sets

#### Open Source Development

https://github.com/backstage/backstage/issues?q=is%3Aissue+is%3Aopen+kubernetes+label%3Ak8s-plugin

### The Plugin in Action

#### Overview

The Kubernetes Plugin is broken into two for convinience:

1. Kubernetes plugin - Ingests information and produces in clean and consistent UI on the Backstage portal
2. Kubernetes Backend plugin - contacts and fetches information via Kubernetes API

#### Installation

##### Installing the front-end plugin

https://backstage.io/docs/features/kubernetes/installation#adding-the-kubernetes-frontend-plugin

##### Installing the backend plugin

https://backstage.io/docs/features/kubernetes/installation#adding-kubernetes-backend-plugin

#### Configuration


https://backstage.io/docs/features/kubernetes/configuration

##### app-config.yaml

```yml
kubernetes:
  objectTypes: ["configmaps","deployments","horizontalpodautoscalers","ingresses","pods","replicasets","services"]
  serviceLocatorMethod:
    type: 'multiTenant'
  clusterLocatorMethods:
    - type: 'config'
      clusters:
        - url: [url of the hosted kubeconfig]
          name: [cluster-name]
          authProvider: ['aws','google','serviceAccount']
          skipTLSVerify: [true/false]
          serviceAccountToken: []
        - ...
    - type: 'gke'
      projectId: []
      region: []
      skipTLSVerify: [true/false]
```

* To see the URL of kubeconfig - `kubectl cluster-info`

##### Service Catalog Components

```yml
annotations:
  'backstage.io/kubernetes-id': []
```

This should match the `label` in the yaml definition of Kubernetes resource we have created.

##### Comments

* Backstage maps resource to catalog components rather than whole cluster.
* Currently, only `multiTenant (a service exists on all the onboarded clusters)` is the only ServiceLocator that is supported - this means that once a catalog is onboarded, it's annotation of Kubernetes ID is matched with all onboarded clusters on Backstage irrespective of whether they are on that cluster on not.

### References

https://roadie.io/backstage/plugins/kubernetes/

#### Creation of Service Account and fetching the tokens

```yml
kind: ServiceAccount
apiVersion: v1
metadata:
  name: backstage
  namespace: default
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: backstage
rules:
  - apiGroups: ["", "apps", "autoscaling", "networking.k8s.io"]
    resources:
      [
        "pods",
        "namespaces",
        "services",
        "configmaps",
        "deployments",
        "replicasets",
        "horizontalpodautoscalers",
        "ingresses",
      ]
    verbs: ["get", "watch", "list"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: backstage
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: backstage
subjects:
- kind: ServiceAccount
  name: backstage
  namespace: default
```

#### Getting the SA Token

```
kubectl get secret $(kubectl get sa <SERVICE_ACCOUNT_NAME> -o=json \
| jq -r '.secrets[0].name') -o=json \
| jq -r '.data["token"]' \
| base64 --decode \
| pbcopy
```

#### Create a Service Entry for Kube SVC API for the cluster in Istio

```yml
kind: ServiceEntry
apiVersion: networking.istio.io/v1alpha3
metadata:
  name: []
spec:
  hosts:
    - []
  ports:
    - name: https
      number: 443 
      protocol: TLS
  location: MESH_EXTERNAL
  resolution: DNS
  exportTo:
    - . 
```

If you are making an MR to onboard your cluster yo, we have to add an Istio Service Entry to hit the API Service Endpoint on the onboarded cluster.

After creating the Istio Egress and before onboarding the EKS cluster in Backstage config, check if the inbound rules are present and we can cURL the to-be onboarded cluster via the deployed Backstage container

```sh
export TOKEN=""
curl -v <API Config endpoint>/openapi/v2 --header "Authorization: Bearer $TOKEN"
```