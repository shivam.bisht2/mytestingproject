# [Backstage](https://backstage.io)

## GitHub vs GitLab Support in Backstage

| **Feature** | **GitHub** | **GitLab** |
| :-- | :-- | :-- |
| View Source | Supported | Supported |
| [TechDocs Integration](https://github.com/backstage/backstage/tree/master/plugins/techdocs-backend) | Supported | Supported |
| [Authentication](https://github.com/backstage/backstage/tree/master/plugins/auth-backend) | Supported | Supported |
| [Identity Resolution](https://github.com/backstage/backstage/issues/6245) | Not Supported | Not Supported |
| Integration for Catalog Reader | Supported | Supported |
| [Organization Onboarding](https://backstage.io/docs/integrations/github/org) | Supported | Not Supported |
| [Discovery Agent](https://backstage.io/docs/integrations/github/discovery) | Supported with a specialized crawler | Not Supported |
| Template Repositories | Supported with Publish Action | Supported with Publish Action |
| CI/CD | [GitHub Actions](https://github.com/backstage/backstage/tree/master/plugins/github-actions) is supported | [GitLab CI](https://github.com/loblaw-sre/backstage-plugin-gitlab) Support is now available |
| Template Repository with built-in CI | Supported | Not Supported |
| Deployments | [Supported](https://github.com/backstage/backstage/tree/master/plugins/github-deployments) | Not supported |
| GitOps - Deployment to EKS via Git Repository | [Supported](https://github.com/backstage/backstage/tree/master/plugins/gitops-profiles) | Not supported |


### Exploring the GitLab Community Plugin

A new [plugin](https://github.com/loblaw-sre/backstage-plugin-gitlab) to support GitLab CI and other utilities was released by the upstream Backstage project on 18th August, 2021.

A POC was done in an isolated repository and following are the findings:

#### How to onboard 

To onboard GitLab repositories in backstage, add following in the catalog-info.yaml

* `kind: Component`
* `metadata.annotations: gitlab.com/project-id: ''`
* `spec.type: service`

#### Features available on the User Interface

![](img/gitlab-plugin-2.png)

![](img/gitlab-plugin-1.png)

#### Drawbacks

* Does not support all types of `spec.type` which can be associated as per Backstage
* It's not specifically GitLab CI Plugin something similar to [GitHub Actions](https://github.com/backstage/backstage/tree/master/plugins/github-actions) that appears in the `CI/CD` TopBar Entity

![](img/gitlab-plugin-drawback.png)

### References

https://github.com/backstage/backstage/tree/master/plugins
