# Debugging developer portal / backstage

Backstage application is a monorepo. A monorepo (mono repository) is **a single repository that stores all of our code and assets**. Using a monorepo is important for many reasons. It creates a single source of truth. It makes it easier to share code. It even makes it easier to refactor code.

Two services running on different ports

-   Front-End  _`localhost:3000`_
-   Back-End  _`localhost:7000`_

## The problem

Usually, node applications can be easily debugged by adding '--inspect'  in the starter scripts in package.json or while running the node command.

The code used in front-end services can be easily debugged using browser tools (shown in the below steps) but backend code can not be debugged and won't be visible in the browser.

We will discuss the steps to smoothly debug our backstage application in visual code and browser.

## Overview

When we use  `yarn dev`  we can debug the front-end code through browser , but as backend service is also running concurrently , it doesnt give us an option to debug , we cannot read the backend code through the browser nor we can use breakpoints in vs code.  
We will be running our backend and frontend instances separately so it can easily be debugged and we can attach breakpoints.

## 1 ) Start the front-end application

-   Step 1:  
    Run  `yarn start`  ; it will load the application on  _localhost:3000_.  
    _Note: If you do not have to debug the backend code, you may directly run  `yarn dev`  , it will start backend and frontend concurrently._
    
-   Step 2:  
    Open developer tools -  **Option + ⌘ + J (on macOS)**, or  **Shift + CTRL + J (on Windows/Linux)**
    
-   Step 3:  
    Go to  **sources**  and navigate to the given below location.
    

![](https://conf.willhillatlas.com/download/attachments/973373506/Screenshot%202021-08-26%20at%202.15.29%20AM.png?version=1&modificationDate=1629955715285&api=v2 "Platform Engineering > Debugging developer portal / backstage > Screenshot 2021-08-26 at 2.15.29 AM.png")

-   Step 4:  
    You will be able to see the front-end code, where you can easily add breakpoints and use other debugging features available on your browser.

![](https://conf.willhillatlas.com/download/attachments/973373506/Screenshot%202021-08-26%20at%202.35.25%20AM.png?version=1&modificationDate=1629955740489&api=v2 "Platform Engineering > Debugging developer portal / backstage > Screenshot 2021-08-26 at 2.35.25 AM.png")

## 2 ) Start the back-end application

  

-   Step 1:  
    Create a  **`./vscode/launch.json`**  file in the root repository.
    
-   Step 2:
    

Write this configuration in  **`launch.json`**

```
{
"version": "0.2.0",
"configurations": [
{
"type": "node",
"cwd": "${workspaceFolder}/packages/backend",
"request": "launch",
"name": "Launch backend",
"runtimeExecutable": "yarn",
"runtimeArgs": [
"start"
],
"skipFiles": [
"<node_internals>/**"
],
}]}
```

-   Step 3:  
    **Run your application**
    To run or debug the backend app in **VS Code**, select **Run and Debug on the Debug start view** or **press F5** and VS Code will try to run your application.
    
    You have to chose **Launch Backend** , when given options.

-   Step 4:  
Attach breakpoints in vs code, you will be able to debug and monitor. 

- Step 5:
Optionally , in **Chrome** , you may even use **Dedicated NodeJs DevTools** , and debug directly in browser. 

Follow these steps-
     Open developer tools -  **Option + ⌘ + J (on macOS)**, or  **Shift + CTRL + J (on Windows/Linux)**
