## Homepage for Backstage

Currently, we don't have a landing page for Backstage and we open up to the Catalog Page which is not a good user experience, especially if you are new to Backstage.

Spotify is currently planning to release their internal Homepage design and UI/UX as an NPM plugin which the adopters can consume.

The [design](https://www.figma.com/file/Ac6usyBGsTnqln7vAFcrFN/Backstage-Design-System-Community?node-id=2185%3A2978) has been teased publicly on [GitHub](https://github.com/backstage/backstage/issues/7249) 

The project [roadmap](https://github.com/backstage/backstage/projects/7#card-69144327) for release of the plugin is also public.

The project will be released in form of this [NPM package](https://www.npmjs.com/package/@backstage/plugin-home)

Spotify's own Backstage Homepage can be viewed [here](https://www.youtube.com/watch?v=UZTVjv-AvZA&t=1787s)

### Best Practices for the HomePage

![](img/homepage.png)

The homepage should have the following:

1. First time users - Tutorial Page

Some guidelines for tutorials:

* Should be less than 5 steps.
* Must have the option to skip.
* Explanatory copy should be less than 3 sentences
* Must include 'back', 'next', and 'finish' buttons.

2. Search Button (Removed from sidebar)

3. Customized Logo for William Hill

4. Customized composable cards

Suggestions:

* Something William Hill Centric?
* Quick Access (Frequently Visited)
* Getting Started
    * Link to Documentation
    * Request Access (Link the Service Team) # Something similar to what [American Airlines](https://user-images.githubusercontent.com/3097461/131389792-2a11d50b-d365-4f85-8bcb-b3876186ecb3.png) does
* Need Help?
    * Request a feature
    * Chat with Us
    * Opt in for Early Alpha - Let them collaborate on SIT and gain feedback

## Structure

From user research done at Spotify's Backstage users, we've found that most users open Backstage already knowing what they want to access.

## General

Guidelines for the homepage

1. Ideally should have a prominent search bar for users to quickly get to what they are looking for
2. Should not be overwhelming - we've found that our users rarely scroll too much on the homepage, so most content should 'above the fold'. We recommend 4 composable cards, maximum.
3. Composable cards can contain a variety of content. They can display data visualization, tasks, issues, links, etc.

## Tutorial

First-time users to Backstage ought to see the tutorial experience. Some guidelines:
1. Should be less than 5 steps.
2. Must have the option to skip.
3. Explanatory copy should be less than 3 sentences
4. Must include 'back', 'next', and 'finish' buttons.

![](img/homepage-to-be.png)
