## Defining standard mkdocs.yml file format

* TechDocs plugin uses Mkdocs to render our documentation files from VCS written in Markdown onto the Backstage central catalog.

* To import the playbooks, write a `mkdocs.yml` at the root of your GitLab repository and link it with a relative path within the `catalog-info.yml`

```yml
metadata:
  annotations:
    backstage.io/techdocs-ref: dir:.
```

* Note: If the `mkdocs.yml` file is at the root of the repository, it indexes file from `docs/` folder present at the root of the repository. 

### Standard Mkdocs file

```yml
site_name: [value]
site_description: [value]

plugins:
  - techdocs-core

nav:
  - [Playbook Title]: '[Relative link to file wrt docs/ folder]'
  - [Heading]:
    - [Playbook Title]: '[Relative link to file wrt docs/ folder]'
  - [Heading]:
    - [Playbook Title]: '[Relative link to file wrt docs/ folder]'
    - [Playbook Title]: '[Relative link to file wrt docs/ folder]'
```

### Example MkDocs file

```yml
site_name: platform-engineering-portal
site_description: Platform Engineering Portal for William Hill built on top of Backstage.io

plugins:
  - techdocs-core

nav:
  - Introduction:
    - Backstage Portal for William Hill: 'index.md'
  - Guides:
    - Setting up Postgres for Backstage: 'Postgres-Setup.md'
    - Spinning on Backstage Application hosted on SCM: 'Getting-Started.md'
    - Dockerizing Backstage Application: 'Dockerizing-Backstage.md' 
    - Standard Repository Catalog YAML: 'repository-catalog.md'
    - API Documentation Catalog: 'api-catalog.md'
```
