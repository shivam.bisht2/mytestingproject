![](onboarding/img/intro.gif)

Backstage is an open platform for building developer portals. It’s based on the developer portal Spotify has been using internally for over four years. Backstage can be as simple as a services catalog or as powerful as the UX layer for your entire tech infrastructure.

Backstage gives teams a very straightforward method to unify all of your infrastructure tooling, services, and documentation under a single, easy-to-use interface. Built around the concept of metadata Yaml files, Backstage makes it easy for a single team to manage tens of services and allows a company to easily manage thousands of them. Because the system is practically self-organizing, it requires considerably less oversight from a centralized Platform team than a normal catalog would. Developers can get a uniform overview of all their software and related resources (such as server utilization, data pipelines, pull request status), regardless of how and where they are running, as well as an easy way to onboard and manage those resources.

Spotify said they reduced onboarding time by more than 50% since introducing Backstage internally. It is no wonder then that ever since the open-source announcement, Backstage has quickly become the go-to framework for most enterprises looking to build a service catalog.

# What is a service catalog

We define a service catalog as a means of centralizing all services that are important to the stakeholders of an organization that implements and uses it. Given its digital implementation, the service catalog acts, at a minimum, as a digital registry and a means for highly distributed enterprises to see, find, invoke, and execute services regardless of where they exist in the company. Crucially, this means that people in one part of the world can find and utilize the same services that people in other teams use on the other side of the world/enterprise, eliminating the need to develop and support local services.

Every service catalog should have some version of the four core elements(further explained below)

## Ownership information and other metadata

A good service catalog contains a range of information about each service in the enterprise. This includes information such as ownership (typically pointing to a specific individual or team), programming language, source code, current version, last update, documentation. Depending on the company, additional information may be essential. This view is especially interesting for the developer or the product manager. It allows anyone in the enterprise to find out very quickly whether a certain required service is already available to then coordinate directly with the respective responsible team.

## Service templating

Engineering teams also use service catalogs as a way to define templates and blueprints for the rest of the engineering organization to use. This allows developers to get coding right away, using a predefined service design and language framework like Java, Scala, Golang, Node.js, etc.

## Service usage

A service catalog answers the question around which service (or fork of it) is consumed by which applications. This view is especially interesting for the team owning said service, as it makes it easy to learn about usage patterns and would allow for a better resource allocation to be cost-effective.

## Service versioning

Finally, the service catalog allows Engineering teams to know at a glance which versions of a particular service are used by which applications and in which environments. This is specifically useful in the event vulnerabilities are found in a given service version, as teams can be warned and only the affected environments or apps can be shut down/rolled back.

A service catalog like Backstage allows you to easily search all your services templates and immediately create a new one if what you are looking for is not available. The new service comes with a predefined design and set of metadata, depending on the specifics of your Ops or Platform team. You can get going with the coding right away, fantastic!

## Benefits of Backstage

For engineering managers, it allows you to maintain standards and best practices across the organization and can help you manage your whole tech ecosystem.

For developers, it makes it fast and simple to build software components in a standardized way, and it provides a central place to manage all projects and documentation.
For platform engineers, it enables extensibility and scalability by letting you easily integrate new tools and services (via plugins), as well as extending the functionality of existing ones.
For Product Owners, it’s a single, consistent experience that ties all your infrastructure tooling, resources, standards, owners, contributors, and administrators together in one place.

We model software in the Backstage catalog using these three core entities (further explained below):

Components are individual pieces of software

APIs are the boundaries between different components

Resources are physical or virtual infrastructure needed to operate a component

![](https://backstage.io/docs/assets/software-catalog/software-model-core-entities.drawio.svg)

## Component

A component is a piece of software, for example, a mobile feature, website, backend service, or data pipeline (list not exhaustive). A component can be tracked in source control, or use some existing open-source or commercial software.

A component can implement APIs for other components to consume. In turn, it might depend on APIs implemented by other components, or resources that are attached to it at runtime.

## API

APIs form an important (maybe the most important) abstraction that allows large software ecosystems to scale. Thus, APIs are a first-class citizen in the Backstage model and the primary way to discover existing functionality in the ecosystem.

APIs are implemented by components and form boundaries between components. They might be defined using an RPC IDL (e.g., Protobuf, GraphQL, ...), HTTP, a data schema (e.g., Avro, TFRecord, ...), or as code interfaces. In any case, APIs exposed by components need to be in a known machine-readable format so we can build further tooling and analysis on top.

There is no distinction in Backstage for Private, restricted, or public APIs. All are public and visible to all users. As public APIs are going to be the primary way of interaction between components, Backstage supports documenting, indexing, and searching all APIs so we can browse them as developers.

## Resource

Resources are the infrastructure a component needs to operate at runtime, like BigTable databases, Pub/Sub topics, S3 buckets, or CDNs. Modeling them together with components and systems will better allow us to visualize resource footprint, and create tooling around them.

## System

Together, the services(sample-backend as shown in the image below) and exposed API (example-API as shown in the image below) make up a logical system. They are a group of entities with a well-defined purpose, providing and managing information.
To represent them as a logical group in Backstage, we can define a system. Systems don’t have types, they are just systems.

```yml
kind: System
name: Sample-System
```

![](onboarding/img/system-image.png)

## Consuming APIs

Of course, it takes more than just a system to make an amazing product.

![multiple-system.png](onboarding/img/multiple-system.png)

The key properties required to represent this in Backstage are as follows:

```yml
kind: System
name: Sample-System
---
kind: Component
type: service
name: sample-backend
consumesApis:
  - example-api
```

When we look up the sample-api API in Backstage, we can now see that the sample-backend is a downstream dependency.

## Ecosystem Modeling

A large catalog of components, APIs, and resources can be highly granular and hard to understand as a whole. It might thus be convenient to further categorize these entities using the following (optional) concepts:

- Systems are a collection of entities that cooperate to perform some function
- Domains relate entities and systems to part of the business

## Domain

While systems are the basic level of encapsulation for related entities, it is often useful to group a collection of systems that share terminology, domain models, metrics, KPIs, business purpose, or documentation, i.e. they form a bounded context.

